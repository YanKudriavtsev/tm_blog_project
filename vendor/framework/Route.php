<?php

namespace Framework;

class Route extends Controller
{
    static function start() {
        Middleware::runMiddleware();

        $url = \UrlHelper::getUrl();
        $routes = require_once ROOT . '/config/routes.php';

        foreach ($routes as $template=>$handlerString) {
            if (preg_match($template, $url)) {
                /*
                 * If the query URL matches the URL pattern, then
                 * formed a line ('controllerName/actionName/param1/param2'),
                 * where params - id, category name, etc.
                 */
                $handlers = explode('/', preg_replace($template, $handlerString, $url));
                $controllerName = array_shift($handlers);
                $actionName = array_shift($handlers);
                $args = $handlers;
            }
        }

        /*
         * If the query URL not matched URL patterns, then "controller" and "action"
         * has be as "controller" and "action" for showing "Page 404"
         */
        if (!isset($controllerName)) {
            $controllerName = 'ErrorController';
            $actionName = 'showAction';
            $args = ['404'];
        }

        call_user_func_array(array(new $controllerName(), $actionName), $args);
    }
}