<?php

namespace Framework;


class Database
{
    private $connect;
    private static $_instance;

    private function __construct()
    {
        $this->connect = new \PDO('mysql:host=' . HOST . ';dbname=' . DB, USER, PASSWORD, array(
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION
        ));
    }

    private function __clone(){}

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getConnect()
    {
        return $this->connect;
    }
}