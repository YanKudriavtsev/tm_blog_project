<?php
namespace Framework;

interface MiddlewareInterface
{
    public static function run();
}

class Middleware
{
    /*
     * This method causing method "run" in all middleware of user
     * ('/application/middleware')
     * Middleware of user use the "MiddlewareInterface"
     */
    public static function runMiddleware()
    {
        /*
        * Iteration on the list of user "middlewares" and
        * causing "run" method in user middleware.
        * User middleware implements "MiddlewareInterface" from this file
        */
        $middlewareList = require_once ROOT . '/config/middlewareList.php';
        foreach ($middlewareList as $middleware) {
            $middleware::run();
        }
    }
}
