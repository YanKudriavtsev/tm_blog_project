<?php
require_once 'config/config.php';

//display errors
ini_set('display_errors', 1);
error_reporting(E_ALL);

session_start();

define('ROOT', __DIR__);
require_once 'vendor/bootstrap.php';
