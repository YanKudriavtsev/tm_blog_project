-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июл 24 2017 г., 14:24
-- Версия сервера: 5.7.19-0ubuntu0.16.04.1
-- Версия PHP: 7.0.18-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `tm_blog_alt`
--

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Astronomy', '2017-07-14 11:07:29', '2017-07-14 11:07:29'),
(2, 'Biology', '2017-07-14 11:07:29', '2017-07-14 11:07:29'),
(3, 'Chemistry', '2017-07-14 11:07:57', '2017-07-14 11:07:57'),
(4, 'Physics', '2017-07-14 11:07:57', '2017-07-14 11:07:57'),
(5, 'Mathematics', '2017-07-14 11:09:00', '2017-07-14 11:09:00'),
(6, 'Programming', '2017-07-22 18:57:45', '2017-07-22 18:57:45'),
(7, 'Space', '2017-07-22 19:00:32', '2017-07-22 19:00:32');

-- --------------------------------------------------------

--
-- Структура таблицы `category_blog_post`
--

CREATE TABLE `category_blog_post` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `category_blog_post`
--

INSERT INTO `category_blog_post` (`id`, `category_id`, `post_id`) VALUES
(5, 5, 1),
(6, 4, 1),
(7, 6, 1),
(8, 7, 1),
(9, 5, 2),
(10, 4, 2),
(11, 6, 2),
(12, 7, 2),
(13, 5, 3),
(14, 6, 3),
(15, 5, 4),
(16, 6, 4),
(17, 5, 5),
(18, 4, 5),
(19, 7, 5);

-- --------------------------------------------------------

--
-- Структура таблицы `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `text` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `reply_to` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `comment`
--

INSERT INTO `comment` (`id`, `author_id`, `post_id`, `text`, `created_at`, `updated_at`, `status`, `reply_to`) VALUES
(1, 1, 2, 'Lorem ipsum dolor sit amet, et brute dicit doming est, ut vix dico malis, veniam graecis scriptorem his no. Ut sonet vocibus dignissim vel, ex has integre placerat interesset, ne tation nostrum delicata pri. At percipitur voluptatibus quo, id his erat ullum maiorum. Congue urbanitas delicatissimi vim no, paulo percipitur et mea.', '2017-07-23 11:26:23', '2017-07-23 11:26:23', 1, NULL),
(2, 1, 2, 'Lorem ipsum dolor sit amet, porro timeam vituperata ei qui, cum sumo omnes te. Te ius persius pertinacia, usu homero civibus suavitate ne, vis saperet suscipit mediocrem ex. Id audire laboramus ius. Ad elitr corpora temporibus nam, id graece aliquando ius.', '2017-07-23 11:26:41', '2017-07-23 11:26:41', 1, NULL),
(3, 3, 2, 'An iudico mollis indoctum vel, mel eu brute insolens praesent, usu essent animal ut. Minim ignota adversarium id sed, pri at duis omittantur consequuntur, duo tale appareat no. In stet propriae voluptaria qui. Animal vocibus feugait no mei, quod facete id pri. Facete accusamus ei his. Ex vocent phaedrum has, sint reque usu ei.', '2017-07-23 11:32:46', '2017-07-23 11:32:46', 1, NULL),
(4, 2, 2, 'At cum tantas pericula, an nam probo fabellas, at fugit sensibus est. Ex zril facete eum, ut eos suscipit maluisset torquatos. Id ius novum laudem populo, aliquid fabellas platonem ut pro. Vel legere eripuit fastidii an, ne tale nusquam usu, an pro utroque sensibus. Eu vel adhuc recteque intellegebat, option accusamus disputationi sed ea. At mea eius possim, at vel brute semper corpora.', '2017-07-23 11:37:04', '2017-07-23 11:37:04', 1, NULL),
(5, 2, 2, 'Repudiare scriptorem theophrastus ne usu, te autem nulla inciderint his, quo solet semper lucilius ut. Mea ei propriae salutatus. Cu vis paulo commune voluptaria, vero appetere vituperatoribus cum in. Has omnes consul no. Eu dicunt appareat sed, mei ut dicunt deseruisse. Vix in option pertinacia eloquentiam. Sed docendi lucilius eu, duo no veri legendos, dicta molestie maluisset id qui.', '2017-07-23 11:37:14', '2017-07-23 11:37:14', 1, NULL),
(6, 2, 2, 'Dolore tractatos ut his. His ut adhuc accommodare, legere disputando no qui. Unum modus feugait ea eos, an essent alterum elaboraret usu, est te tollit populo commune. Aeterno rationibus mel at, no duo praesent assentior. Cu mei zril soleat signiferumque.', '2017-07-23 11:37:27', '2017-07-23 11:37:27', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `likes_of_posts`
--

CREATE TABLE `likes_of_posts` (
  `id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `likes_of_posts`
--

INSERT INTO `likes_of_posts` (`id`, `owner_id`, `post_id`) VALUES
(2, 1, 1),
(3, 1, 2),
(4, 3, 1),
(5, 3, 2),
(6, 2, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `notification`
--

CREATE TABLE `notification` (
  `id` int(11) NOT NULL,
  `target_id` int(11) NOT NULL,
  `text` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `revised` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `notification`
--

INSERT INTO `notification` (`id`, `target_id`, `text`, `created_at`, `revised`) VALUES
(1, 1, 'New post - <a href=\'/post/5\'>SpaceX to Send Privately Crewed Dragon Spacecraft Beyond the Moon Next Year</a>', '2017-07-23 11:39:40', 1),
(2, 3, 'New post - <a href=\'/post/5\'>SpaceX to Send Privately Crewed Dragon Spacecraft Beyond the Moon Next Year</a>', '2017-07-23 11:39:40', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `short_content` text NOT NULL,
  `text` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tags` text,
  `author` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `post`
--

INSERT INTO `post` (`id`, `title`, `short_content`, `text`, `created_at`, `updated_at`, `tags`, `author`, `status`) VALUES
(1, 'Reusability: The Key to Making Human Life Multi-Planetary', '<p style="text-align:justify">&ldquo;If one can figure out how to effectively reuse rockets just like airplanes, the cost of access to space will be reduced by as much as a factor of a hundred.&nbsp; A fully reusable vehicle has never been done before. That really is the fundamental breakthrough needed to revolutionize access to space.&rdquo;</p>\r\n\r\n<p style="text-align:justify">--Elon Musk</p>\r\n\r\n<p style="text-align:justify">SpaceX believes a fully and rapidly reusable rocket is the pivotal breakthrough needed to substantially reduce the cost of space access. &nbsp;The majority of the launch cost comes from building the rocket, which flies only once. Compare that to a commercial airliner &ndash; each new plane costs about the same as Falcon 9, but can fly multiple times per day, and conduct tens of thousands of flights over its lifetime. Following the commercial model, a rapidly reusable space launch vehicle could reduce the cost of traveling to space by a hundredfold.</p>\r\n\r\n<p styl...', '<p style="text-align:justify"><em>&ldquo;If one can figure out how to effectively reuse rockets just like airplanes, the cost of access to space will be reduced by as much as a factor of a hundred.&nbsp; A fully reusable vehicle has never been done before. That really is the fundamental breakthrough needed to revolutionize access to space.&rdquo;</em></p>\r\n\r\n<p style="text-align:justify">--Elon Musk</p>\r\n\r\n<p style="text-align:justify">SpaceX believes a fully and rapidly reusable rocket is the pivotal breakthrough needed to substantially reduce the cost of space access. &nbsp;The majority of the launch cost comes from building the rocket, which flies only once. Compare that to a commercial airliner &ndash; each new plane costs about the same as Falcon 9, but can fly multiple times per day, and conduct tens of thousands of flights over its lifetime. Following the commercial model, a rapidly reusable space launch vehicle could reduce the cost of traveling to space by a hundredfold.</p>\r\n\r\n<p style="text-align:justify">While most rockets are designed to burn up on reentry, SpaceX rockets can not only withstand reentry, but can also successfuly land back on Earth and refly again.&nbsp;</p>\r\n\r\n<p style="text-align:justify"><strong>World&rsquo;s First Orbital-Class Rocket Reflight</strong></p>\r\n\r\n<p style="text-align:justify">In March 2017, SpaceX achieved <a href="https://youtu.be/xsZSXav4wI8"> the world&rsquo;s first reflight of an orbital class rocket</a>. SpaceX&rsquo;s Falcon 9 rocket launched a geosynchronous communications satellite on March 30, 2017, from Launch Complex 39A (LC-39A) at NASA&#39;s Kennedy Space Center in Florida. The first stage for the mission previously supported a space station cargo resupply launch for NASA in April 2016. Following stage separation, the first stage successfully returned to Earth for a second time, landing on a drone ship stationed in the Atlantic ocean. This successful reflight represents a historic milestone on the road to full and rapid rocket reusability.</p>\r\n\r\n<p style="text-align:justify"><strong>First Stage Landings</strong></p>\r\n\r\n<p style="text-align:justify">Prior to the firest reflight of a Falcon 9, SpaceX successfully landed multiple rocket first stages. On December 21, 2015, Falcon 9 delivered 11 communications satellites to orbit, and the first stage returned and landed at Landing Zone 1 -- the first-ever <a href="http://www.spacex.com/news/2015/12/21/background-tonights-launch">orbital class rocket landing</a>.</p>\r\n\r\n<p style="text-align:justify"><img alt="" src="http://www.spacex.com/sites/spacex/files/orbcommlanding.jpg" style="height:278px; width:500px" /></p>\r\n\r\n<p style="text-align:justify">Then, on April 8, 2016, during a <a href="http://www.spacex.com/news/2016/04/09/crs-8-launch-and-landing">resupply mission for NASA</a>, a Falcon 9 first stage successfully landed on SpaceX&rsquo;s autonomous spaceport drone ship in the Atlantic Ocean. This first stage was notably reflown in March 2017, representing the first-ever reflight of an orbital-class rocket stage. The <a href="http://www.spacex.com/news/2014/12/16/x-marks-spot-falcon-9-attempts-ocean-platform-landing">ability to recover first stages at sea</a> is an important component of SpaceX&rsquo;s reusability program because certain missions do not leave enough fuel margin for the first stage to return all the way back to land.</p>\r\n\r\n<p style="text-align:justify">SpaceX will continue to attempt landing Falcon 9 rockets either on land or on the drone ship at sea on almost all missions going forward. These technical achievements are made possible by innovative engineering upgrades to the vehicle, including <a href="http://www.spacex.com/news/2015/06/24/why-and-how-landing-rockets">grid fins, cold-gas thrusters, and landing legs.</a></p>\r\n\r\n<p style="text-align:justify"><strong>Ocean Landings</strong></p>\r\n\r\n<p style="text-align:justify">Prior to successfully landing a Falcon 9 first stage, SpaceX had twice reentered a Falcon 9 first stage from space and landed it in the ocean. From there, SpaceX moved on to attempt using the drone ship as a landing platform during January and April 2015 missions. While the rocket did not stick the landing on these first two attempts, SpaceX gathered important data each time that would ultimately lead to a successful landing.</p>\r\n\r\n<p style="text-align:justify"><strong>Grasshopper and F9R Test Programs</strong></p>\r\n\r\n<p style="text-align:justify">SpaceX&rsquo;s initial reusability tests using the Grasshopper and F9R test vehicles took place in 2012&ndash;2014 at SpaceX&rsquo;s test facility in McGregor, Texas. The Grasshopper Vertical Take Off, Vertical Landing (VTVL) vehicle was essentially a Falcon 9 first stage with one Merlin 1D engine and attached steel landing legs. In 2012&ndash;2013, Grasshopper completed a series of eight flight tests with landings, the highest reaching 744 meters high. Following the retirement of Grasshopper, SpaceX began testing the F9R development vehicle, which had three Merlin 1D engines for additional thrust. F9R completed successively higher tests in 2014 topping out with a 1000m test using steerable grid fins. These overland tests provided invaluable information for future flight testing during orbital missions, ultimately leading to the first rocket landing in 2015.</p>\r\n', '2017-07-23 11:17:16', '2017-07-23 11:21:10', 'SpaceX ElonMusk Space', 2, 1),
(2, 'First Dragon Reflight', '<p style="text-align: justify;">On June 3, 2017, SpaceX&rsquo;s Falcon 9 rocket successfully launched a Dragon spacecraft for the company&rsquo;s eleventh Commercial Resupply Services mission (CRS-11) to the International Space Station.</p>\r\n\r\n<p style="text-align: justify;">This mission marked the first reflight of a Dragon spacecraft, having previously flown during the fourth Commercial Resupply Services (CRS-4) mission back in September 2014.</p>\r\n\r\n<p style="text-align: justify;">This launch also marked the 100th launch from historic Launch Complex 39A (LC-39A) at NASA&rsquo;s Kennedy Space Center. Previous launches include 11 Apollo flights, the launch of the un-crewed Skylab in 1973, 82 shuttle flights, and five&nbsp;SpaceX&nbsp;launches.</p>\r\n\r\n<p style="text-align: justify;">Following stage separation, the first stage of Falcon 9 successfully landed at SpaceX&rsquo;s Landing Zone 1 (LZ-1) at Cape Canaveral Air Force Station, Florida.</p>\r\n\r\n<p style="text-align: justify;">Drago...', '<p style="text-align: justify;">On June 3, 2017, SpaceX&rsquo;s Falcon 9 rocket successfully launched a Dragon spacecraft for the company&rsquo;s eleventh Commercial Resupply Services mission (CRS-11) to the International Space Station.</p>\r\n\r\n<p style="text-align: justify;">This mission marked the first reflight of a Dragon spacecraft, having previously flown during the fourth Commercial Resupply Services (CRS-4) mission back in September 2014.</p>\r\n\r\n<p style="text-align: justify;">This launch also marked the 100th launch from historic Launch Complex 39A (LC-39A) at NASA&rsquo;s Kennedy Space Center. Previous launches include 11 Apollo flights, the launch of the un-crewed Skylab in 1973, 82 shuttle flights, and five&nbsp;SpaceX&nbsp;launches.</p>\r\n\r\n<p style="text-align: justify;">Following stage separation, the first stage of Falcon 9 successfully landed at SpaceX&rsquo;s Landing Zone 1 (LZ-1) at Cape Canaveral Air Force Station, Florida.</p>\r\n\r\n<p style="text-align: justify;">Dragon is scheduled to arrive at the International Space Station on Monday, June 5 to be installed on the Earth-facing side of the Harmony module for its month-long stay. You can watch the archived mission webcast below, and find more information about the mission in our <a href="http://www.spacex.com/sites/spacex/files/crs11finalpresskit.pdf">press kit</a>. Additional photos are also available on our <a href="https://www.flickr.com/photos/spacex">Flickr page</a>.</p>\r\n', '2017-07-23 11:24:20', '2017-07-23 11:24:20', 'SpaceX ElonMusk Space Dragon', 2, 1),
(3, 'What is free software?', 'The Free Software Definition\r\n\r\n\r\n<p style="text-align: justify;">The free software definition presents the criteria for whether a particular software program qualifies as free software. From time to time we revise this definition, to clarify it or to resolve questions about subtle issues. See the History section below for a list of changes that affect the definition of free software.</p>\r\n\r\n\r\n<p style="text-align: justify;">&ldquo;Free software&rdquo; means software that respects users&#39; freedom and community. Roughly, it means that the users have the freedom to run, copy, distribute, study, change and improve the software. Thus, &ldquo;free software&rdquo; is a matter of liberty, not price. To understand the concept, you should think of &ldquo;free&rdquo; as in &ldquo;free speech,&rdquo; not as in &ldquo;free beer&rdquo;. We sometimes call it &ldquo;libre software,&rdquo; borrowing the French or Spanish word for &ldquo;free&rdquo; as in freedom, to show we do not mean the software...', '<h3 style="text-align: justify;">The Free Software Definition</h3>\r\n\r\n<blockquote>\r\n<p style="text-align: justify;">The free software definition presents the criteria for whether a particular software program qualifies as free software. From time to time we revise this definition, to clarify it or to resolve questions about subtle issues. See the <a href="https://www.gnu.org/philosophy/free-sw.html#History">History section</a> below for a list of changes that affect the definition of free software.</p>\r\n</blockquote>\r\n\r\n<p style="text-align: justify;">&ldquo;Free software&rdquo; means software that respects users&#39; freedom and community. Roughly, it means that <strong>the users have the freedom to run, copy, distribute, study, change and improve the software</strong>. Thus, &ldquo;free software&rdquo; is a matter of liberty, not price. To understand the concept, you should think of &ldquo;free&rdquo; as in &ldquo;free speech,&rdquo; not as in &ldquo;free beer&rdquo;. We sometimes call it &ldquo;libre software,&rdquo; borrowing the French or Spanish word for &ldquo;free&rdquo; as in freedom, to show we do not mean the software is gratis.</p>\r\n\r\n<p style="text-align: justify;">We campaign for these freedoms because everyone deserves them. With these freedoms, the users (both individually and collectively) control the program and what it does for them. When users don&#39;t control the program, we call it a &ldquo;nonfree&rdquo; or &ldquo;proprietary&rdquo; program. The nonfree program controls the users, and the developer controls the program; this makes the program <a href="https://www.gnu.org/philosophy/free-software-even-more-important.html"> an instrument of unjust power</a>.</p>\r\n\r\n<p style="text-align: justify;">The four essential freedoms</p>\r\n\r\n<p style="text-align: justify;">A program is free software if the program&#39;s users have the four essential freedoms:</p>\r\n\r\n<ul>\r\n	<li style="text-align: justify;">The freedom to run the program as you wish, for any purpose (freedom 0).</li>\r\n	<li style="text-align: justify;">The freedom to study how the program works, and change it so it does your computing as you wish (freedom 1). Access to the source code is a precondition for this.</li>\r\n	<li style="text-align: justify;">The freedom to redistribute copies so you can help your neighbor (freedom 2).</li>\r\n	<li style="text-align: justify;">The freedom to distribute copies of your modified versions to others (freedom 3). By doing this you can give the whole community a chance to benefit from your changes. Access to the source code is a precondition for this.</li>\r\n</ul>\r\n\r\n<p style="text-align: justify;">A program is free software if it gives users adequately all of these freedoms. Otherwise, it is nonfree. While we can distinguish various nonfree distribution schemes in terms of how far they fall short of being free, we consider them all equally unethical.</p>\r\n\r\n<p style="text-align: justify;">In any given scenario, these freedoms must apply to whatever code we plan to make use of, or lead others to make use of. For instance, consider a program A which automatically launches a program B to handle some cases. If we plan to distribute A as it stands, that implies users will need B, so we need to judge whether both A and B are free. However, if we plan to modify A so that it doesn&#39;t use B, only A needs to be free; B is not pertinent to that plan.</p>\r\n\r\n<p style="text-align: justify;">&ldquo;Free software&rdquo; does not mean &ldquo;noncommercial&rdquo;. A free program must be available for commercial use, commercial development, and commercial distribution. Commercial development of free software is no longer unusual; such free commercial software is very important. You may have paid money to get copies of free software, or you may have obtained copies at no charge. But regardless of how you got your copies, you always have the freedom to copy and change the software, even to <a href="https://www.gnu.org/philosophy/selling.html">sell copies</a>.</p>\r\n\r\n<p style="text-align: justify;">The rest of this page clarifies certain points about what makes specific freedoms adequate or not.</p>\r\n\r\n<p style="text-align: justify;">The freedom to run the program as you wish</p>\r\n\r\n<p style="text-align: justify;">The freedom to run the program means the freedom for any kind of person or organization to use it on any kind of computer system, for any kind of overall job and purpose, without being required to communicate about it with the developer or any other specific entity. In this freedom, it is the <em>user&#39;s</em> purpose that matters, not the <em>developer&#39;s</em> purpose; you as a user are free to run the program for your purposes, and if you distribute it to someone else, she is then free to run it for her purposes, but you are not entitled to impose your purposes on her.</p>\r\n\r\n<p style="text-align: justify;">The freedom to run the program as you wish means that you are not forbidden or stopped from making it run. This has nothing to do with what functionality the program has, whether it is technically capable of functioning in any given environment, or whether it is useful for any particular computing activity.</p>\r\n\r\n<p style="text-align: justify;">The freedom to study the source code and make changes</p>\r\n\r\n<p style="text-align: justify;">In order for freedoms 1 and 3 (the freedom to make changes and the freedom to publish the changed versions) to be meaningful, you must have access to the source code of the program. Therefore, accessibility of source code is a necessary condition for free software. Obfuscated &ldquo;source code&rdquo; is not real source code and does not count as source code.</p>\r\n\r\n<p style="text-align: justify;">Freedom 1 includes the freedom to use your changed version in place of the original. If the program is delivered in a product designed to run someone else&#39;s modified versions but refuse to run yours &mdash; a practice known as &ldquo;tivoization&rdquo; or &ldquo;lockdown&rdquo;, or (in its practitioners&#39; perverse terminology) as &ldquo;secure boot&rdquo; &mdash; freedom 1 becomes an empty pretense rather than a practical reality. These binaries are not free software even if the source code they are compiled from is free.</p>\r\n\r\n<p style="text-align: justify;">One important way to modify a program is by merging in available free subroutines and modules. If the program&#39;s license says that you cannot merge in a suitably licensed existing module &mdash; for instance, if it requires you to be the copyright holder of any code you add &mdash; then the license is too restrictive to qualify as free.</p>\r\n\r\n<p style="text-align: justify;">Whether a change constitutes an improvement is a subjective matter. If your right to modify a program is limited, in substance, to changes that someone else considers an improvement, that program is not free.</p>\r\n\r\n<p style="text-align: justify;">The freedom to redistribute if you wish: basic requirements</p>\r\n\r\n<p style="text-align: justify;">Freedom to distribute (freedoms 2 and 3) means you are free to redistribute copies, either with or without modifications, either gratis or charging a fee for distribution, to <a href="https://www.gnu.org/philosophy/free-sw.html#exportcontrol">anyone anywhere</a>. Being free to do these things means (among other things) that you do not have to ask or pay for permission to do so.</p>\r\n\r\n<p style="text-align: justify;">You should also have the freedom to make modifications and use them privately in your own work or play, without even mentioning that they exist. If you do publish your changes, you should not be required to notify anyone in particular, or in any particular way.</p>\r\n\r\n<p style="text-align: justify;">Freedom 3 includes the freedom to release your modified versions as free software. A free license may also permit other ways of releasing them; in other words, it does not have to be a <a href="https://www.gnu.org/copyleft/copyleft.html">copyleft</a> license. However, a license that requires modified versions to be nonfree does not qualify as a free license.</p>\r\n\r\n<p style="text-align: justify;">The freedom to redistribute copies must include binary or executable forms of the program, as well as source code, for both modified and unmodified versions. (Distributing programs in runnable form is necessary for conveniently installable free operating systems.) It is OK if there is no way to produce a binary or executable form for a certain program (since some languages don&#39;t support that feature), but you must have the freedom to redistribute such forms should you find or develop a way to make them.</p>\r\n\r\n<p style="text-align: justify;">Copyleft</p>\r\n\r\n<p style="text-align: justify;">Certain kinds of rules about the manner of distributing free software are acceptable, when they don&#39;t conflict with the central freedoms. For example, <a href="https://www.gnu.org/copyleft/copyleft.html">copyleft</a> (very simply stated) is the rule that when redistributing the program, you cannot add restrictions to deny other people the central freedoms. This rule does not conflict with the central freedoms; rather it protects them.</p>\r\n\r\n<p style="text-align: justify;">In the GNU project, we use copyleft to protect the four freedoms legally for everyone. We believe there are important reasons why <a href="https://www.gnu.org/philosophy/pragmatic.html">it is better to use copyleft</a>. However, <a href="https://www.gnu.org/philosophy/categories.html#Non-CopyleftedFreeSoftware"> noncopylefted free software</a> is ethical too. See <a href="https://www.gnu.org/philosophy/categories.html">Categories of Free Software</a> for a description of how &ldquo;free software,&rdquo; &ldquo;copylefted software&rdquo; and other categories of software relate to each other.</p>\r\n\r\n<p style="text-align: justify;">Rules about packaging and distribution details</p>\r\n\r\n<p style="text-align: justify;">Rules about how to package a modified version are acceptable, if they don&#39;t substantively limit your freedom to release modified versions, or your freedom to make and use modified versions privately. Thus, it is acceptable for the license to require that you change the name of the modified version, remove a logo, or identify your modifications as yours. As long as these requirements are not so burdensome that they effectively hamper you from releasing your changes, they are acceptable; you&#39;re already making other changes to the program, so you won&#39;t have trouble making a few more.</p>\r\n\r\n<p style="text-align: justify;">Rules that &ldquo;if you make your version available in this way, you must make it available in that way also&rdquo; can be acceptable too, on the same condition. An example of such an acceptable rule is one saying that if you have distributed a modified version and a previous developer asks for a copy of it, you must send one. (Note that such a rule still leaves you the choice of whether to distribute your version at all.) Rules that require release of source code to the users for versions that you put into public use are also acceptable.</p>\r\n\r\n<p style="text-align: justify;">A special issue arises when a license requires changing the name by which the program will be invoked from other programs. That effectively hampers you from releasing your changed version so that it can replace the original when invoked by those other programs. This sort of requirement is acceptable only if there&#39;s a suitable aliasing facility that allows you to specify the original program&#39;s name as an alias for the modified version.</p>\r\n\r\n<p style="text-align: justify;">Export regulations</p>\r\n\r\n<p style="text-align: justify;">Sometimes government export control regulations and trade sanctions can constrain your freedom to distribute copies of programs internationally. Software developers do not have the power to eliminate or override these restrictions, but what they can and must do is refuse to impose them as conditions of use of the program. In this way, the restrictions will not affect activities and people outside the jurisdictions of these governments. Thus, free software licenses must not require obedience to any nontrivial export regulations as a condition of exercising any of the essential freedoms.</p>\r\n\r\n<p style="text-align: justify;">Merely mentioning the existence of export regulations, without making them a condition of the license itself, is acceptable since it does not restrict users. If an export regulation is actually trivial for free software, then requiring it as a condition is not an actual problem; however, it is a potential problem, since a later change in export law could make the requirement nontrivial and thus render the software nonfree.</p>\r\n\r\n<p style="text-align: justify;">Legal considerations</p>\r\n\r\n<p style="text-align: justify;">In order for these freedoms to be real, they must be permanent and irrevocable as long as you do nothing wrong; if the developer of the software has the power to revoke the license, or retroactively add restrictions to its terms, without your doing anything wrong to give cause, the software is not free.</p>\r\n\r\n<p style="text-align: justify;">A free license may not require compliance with the license of a nonfree program. Thus, for instance, if a license requires you to comply with the licenses of &ldquo;all the programs you use&rdquo;, in the case of a user that runs nonfree programs this would require compliance with the licenses of those nonfree programs; that makes the license nonfree.</p>\r\n\r\n<p style="text-align: justify;">It is acceptable for a free license to specify which jurisdiction&#39;s law applies, or where litigation must be done, or both.</p>\r\n\r\n<p style="text-align: justify;">Contract-based licenses</p>\r\n\r\n<p style="text-align: justify;">Most free software licenses are based on copyright, and there are limits on what kinds of requirements can be imposed through copyright. If a copyright-based license respects freedom in the ways described above, it is unlikely to have some other sort of problem that we never anticipated (though this does happen occasionally). However, some free software licenses are based on contracts, and contracts can impose a much larger range of possible restrictions. That means there are many possible ways such a license could be unacceptably restrictive and nonfree.</p>\r\n\r\n<p style="text-align: justify;">We can&#39;t possibly list all the ways that might happen. If a contract-based license restricts the user in an unusual way that copyright-based licenses cannot, and which isn&#39;t mentioned here as legitimate, we will have to think about it, and we will probably conclude it is nonfree.</p>\r\n\r\n<p style="text-align: justify;">Use the right words when talking about free software</p>\r\n\r\n<p style="text-align: justify;">When talking about free software, it is best to avoid using terms like &ldquo;give away&rdquo; or &ldquo;for free,&rdquo; because those terms imply that the issue is about price, not freedom. Some common terms such as &ldquo;piracy&rdquo; embody opinions we hope you won&#39;t endorse. See <a href="https://www.gnu.org/philosophy/words-to-avoid.html">Confusing Words and Phrases that are Worth Avoiding</a> for a discussion of these terms. We also have a list of proper <a href="https://www.gnu.org/philosophy/fs-translations.html">translations of &ldquo;free software&rdquo;</a> into various languages.</p>\r\n\r\n<p style="text-align: justify;">How we interpret these criteria</p>\r\n\r\n<p style="text-align: justify;">Finally, note that criteria such as those stated in this free software definition require careful thought for their interpretation. To decide whether a specific software license qualifies as a free software license, we judge it based on these criteria to determine whether it fits their spirit as well as the precise words. If a license includes unconscionable restrictions, we reject it, even if we did not anticipate the issue in these criteria. Sometimes a license requirement raises an issue that calls for extensive thought, including discussions with a lawyer, before we can decide if the requirement is acceptable. When we reach a conclusion about a new issue, we often update these criteria to make it easier to see why certain licenses do or don&#39;t qualify.</p>\r\n\r\n<p style="text-align: justify;">Get help with free licenses</p>\r\n\r\n<p style="text-align: justify;">If you are interested in whether a specific license qualifies as a free software license, see our <a href="https://www.gnu.org/licenses/license-list.html">list of licenses</a>. If the license you are concerned with is not listed there, you can ask us about it by sending us email at <a href="mailto:licensing@gnu.org">&lt;licensing@gnu.org&gt;</a>.</p>\r\n\r\n<p style="text-align: justify;">If you are contemplating writing a new license, please contact the Free Software Foundation first by writing to that address. The proliferation of different free software licenses means increased work for users in understanding the licenses; we may be able to help you find an existing free software license that meets your needs.</p>\r\n\r\n<p style="text-align: justify;">If that isn&#39;t possible, if you really need a new license, with our help you can ensure that the license really is a free software license and avoid various practical problems.</p>\r\n\r\n<h3 style="text-align: justify;">Beyond Software</h3>\r\n\r\n<p style="text-align: justify;"><a href="https://www.gnu.org/philosophy/free-doc.html">Software manuals must be free</a>, for the same reasons that software must be free, and because the manuals are in effect part of the software.</p>\r\n\r\n<p style="text-align: justify;">The same arguments also make sense for other kinds of works of practical use &mdash; that is to say, works that embody useful knowledge, such as educational works and reference works. <a href="http://wikipedia.org">Wikipedia</a> is the best-known example.</p>\r\n\r\n<p style="text-align: justify;">Any kind of work <em>can</em> be free, and the definition of free software has been extended to a definition of <a href="http://freedomdefined.org/"> free cultural works</a> applicable to any kind of works.</p>\r\n\r\n<h3 style="text-align: justify;">Open Source?</h3>\r\n\r\n<p style="text-align: justify;">Another group uses the term &ldquo;open source&rdquo; to mean something close (but not identical) to &ldquo;free software&rdquo;. We prefer the term &ldquo;free software&rdquo; because, once you have heard that it refers to freedom rather than price, it calls to mind freedom. The word &ldquo;open&rdquo; <a href="https://www.gnu.org/philosophy/open-source-misses-the-point.html"> never refers to freedom</a>.</p>\r\n\r\n<h3 style="text-align: justify;">History</h3>\r\n\r\n<p style="text-align: justify;">From time to time we revise this Free Software Definition. Here is the list of substantive changes, along with links to show exactly what was changed.</p>\r\n\r\n<ul>\r\n	<li style="text-align: justify;"><a href="http://web.cvs.savannah.gnu.org/viewvc/www/philosophy/free-sw.html?root=www&amp;r1=1.152&amp;r2=1.153">Version 1.153</a>: Clarify that freedom to run the program means nothing stops you from making it run.</li>\r\n	<li style="text-align: justify;"><a href="http://web.cvs.savannah.gnu.org/viewvc/www/philosophy/free-sw.html?root=www&amp;r1=1.140&amp;r2=1.141">Version 1.141</a>: Clarify which code needs to be free.</li>\r\n	<li style="text-align: justify;"><a href="http://web.cvs.savannah.gnu.org/viewvc/www/philosophy/free-sw.html?root=www&amp;r1=1.134&amp;r2=1.135">Version 1.135</a>: Say each time that freedom 0 is the freedom to run the program as you wish.</li>\r\n	<li style="text-align: justify;"><a href="http://web.cvs.savannah.gnu.org/viewvc/www/philosophy/free-sw.html?root=www&amp;r1=1.133&amp;r2=1.134">Version 1.134</a>: Freedom 0 is not a matter of the program&#39;s functionality.</li>\r\n	<li style="text-align: justify;"><a href="http://web.cvs.savannah.gnu.org/viewvc/www/philosophy/free-sw.html?root=www&amp;r1=1.130&amp;r2=1.131">Version 1.131</a>: A free license may not require compliance with a nonfree license of another program.</li>\r\n	<li style="text-align: justify;"><a href="http://web.cvs.savannah.gnu.org/viewvc/www/philosophy/free-sw.html?root=www&amp;r1=1.128&amp;r2=1.129">Version 1.129</a>: State explicitly that choice of law and choice of forum specifications are allowed. (This was always our policy.)</li>\r\n	<li style="text-align: justify;"><a href="http://web.cvs.savannah.gnu.org/viewvc/www/philosophy/free-sw.html?root=www&amp;r1=1.121&amp;r2=1.122">Version 1.122</a>: An export control requirement is a real problem if the requirement is nontrivial; otherwise it is only a potential problem.</li>\r\n	<li style="text-align: justify;"><a href="http://web.cvs.savannah.gnu.org/viewvc/www/philosophy/free-sw.html?root=www&amp;r1=1.117&amp;r2=1.118">Version 1.118</a>: Clarification: the issue is limits on your right to modify, not on what modifications you have made. And modifications are not limited to &ldquo;improvements&rdquo;</li>\r\n	<li style="text-align: justify;"><a href="http://web.cvs.savannah.gnu.org/viewvc/www/philosophy/free-sw.html?root=www&amp;r1=1.110&amp;r2=1.111">Version 1.111</a>: Clarify 1.77 by saying that only retroactive <em>restrictions</em> are unacceptable. The copyright holders can always grant additional <em>permission</em> for use of the work by releasing the work in another way in parallel.</li>\r\n	<li style="text-align: justify;"><a href="http://web.cvs.savannah.gnu.org/viewvc/www/philosophy/free-sw.html?root=www&amp;r1=1.104&amp;r2=1.105">Version 1.105</a>: Reflect, in the brief statement of freedom 1, the point (already stated in version 1.80) that it includes really using your modified version for your computing.</li>\r\n	<li style="text-align: justify;"><a href="http://web.cvs.savannah.gnu.org/viewvc/www/philosophy/free-sw.html?root=www&amp;r1=1.91&amp;r2=1.92">Version 1.92</a>: Clarify that obfuscated code does not qualify as source code.</li>\r\n	<li style="text-align: justify;"><a href="http://web.cvs.savannah.gnu.org/viewvc/www/philosophy/free-sw.html?root=www&amp;r1=1.89&amp;r2=1.90">Version 1.90</a>: Clarify that freedom 3 means the right to distribute copies of your own modified or improved version, not a right to participate in someone else&#39;s development project.</li>\r\n	<li style="text-align: justify;"><a href="http://web.cvs.savannah.gnu.org/viewvc/www/philosophy/free-sw.html?root=www&amp;r1=1.88&amp;r2=1.89">Version 1.89</a>: Freedom 3 includes the right to release modified versions as free software.</li>\r\n	<li style="text-align: justify;"><a href="http://web.cvs.savannah.gnu.org/viewvc/www/philosophy/free-sw.html?root=www&amp;r1=1.79&amp;r2=1.80">Version 1.80</a>: Freedom 1 must be practical, not just theoretical; i.e., no tivoization.</li>\r\n	<li style="text-align: justify;"><a href="http://web.cvs.savannah.gnu.org/viewvc/www/philosophy/free-sw.html?root=www&amp;r1=1.76&amp;r2=1.77">Version 1.77</a>: Clarify that all retroactive changes to the license are unacceptable, even if it&#39;s not described as a complete replacement.</li>\r\n	<li style="text-align: justify;"><a href="http://web.cvs.savannah.gnu.org/viewvc/www/philosophy/free-sw.html?root=www&amp;r1=1.73&amp;r2=1.74">Version 1.74</a>: Four clarifications of points not explicit enough, or stated in some places but not reflected everywhere:\r\n	<ul>\r\n		<li>&quot;Improvements&quot; does not mean the license can substantively limit what kinds of modified versions you can release. Freedom 3 includes distributing modified versions, not just changes.</li>\r\n		<li>The right to merge in existing modules refers to those that are suitably licensed.</li>\r\n		<li>Explicitly state the conclusion of the point about export controls.</li>\r\n		<li>Imposing a license change constitutes revoking the old license.</li>\r\n	</ul>\r\n	</li>\r\n	<li style="text-align: justify;"><a href="http://web.cvs.savannah.gnu.org/viewvc/www/philosophy/free-sw.html?root=www&amp;r1=1.56&amp;r2=1.57">Version 1.57</a>: Add &quot;Beyond Software&quot; section.</li>\r\n	<li style="text-align: justify;"><a href="http://web.cvs.savannah.gnu.org/viewvc/www/philosophy/free-sw.html?root=www&amp;r1=1.45&amp;r2=1.46">Version 1.46</a>: Clarify whose purpose is significant in the freedom to run the program for any purpose.</li>\r\n	<li style="text-align: justify;"><a href="http://web.cvs.savannah.gnu.org/viewvc/www/philosophy/free-sw.html?root=www&amp;r1=1.40&amp;r2=1.41">Version 1.41</a>: Clarify wording about contract-based licenses.</li>\r\n	<li style="text-align: justify;"><a href="http://web.cvs.savannah.gnu.org/viewvc/www/philosophy/free-sw.html?root=www&amp;r1=1.39&amp;r2=1.40">Version 1.40</a>: Explain that a free license must allow to you use other available free software to create your modifications.</li>\r\n	<li style="text-align: justify;"><a href="http://web.cvs.savannah.gnu.org/viewvc/www/philosophy/free-sw.html?root=www&amp;r1=1.38&amp;r2=1.39">Version 1.39</a>: Note that it is acceptable for a license to require you to provide source for versions of the software you put into public use.</li>\r\n	<li style="text-align: justify;"><a href="http://web.cvs.savannah.gnu.org/viewvc/www/philosophy/free-sw.html?root=www&amp;r1=1.30&amp;r2=1.31">Version 1.31</a>: Note that it is acceptable for a license to require you to identify yourself as the author of modifications. Other minor clarifications throughout the text.</li>\r\n	<li style="text-align: justify;"><a href="http://web.cvs.savannah.gnu.org/viewvc/www/philosophy/free-sw.html?root=www&amp;r1=1.22&amp;r2=1.23">Version 1.23</a>: Address potential problems related to contract-based licenses.</li>\r\n	<li style="text-align: justify;"><a href="http://web.cvs.savannah.gnu.org/viewvc/www/philosophy/free-sw.html?root=www&amp;r1=1.15&amp;r2=1.16">Version 1.16</a>: Explain why distribution of binaries is important.</li>\r\n	<li style="text-align: justify;"><a href="http://web.cvs.savannah.gnu.org/viewvc/www/philosophy/free-sw.html?root=www&amp;r1=1.10&amp;r2=1.11">Version 1.11</a>: Note that a free license may require you to send a copy of versions you distribute to previous developers on request.</li>\r\n</ul>\r\n\r\n<p style="text-align: justify;">There are gaps in the version numbers shown above because there are other changes in this page that do not affect the definition or its interpretations. For instance, the list does not include changes in asides, formatting, spelling, punctuation, or other parts of the page. You can review the complete list of changes to the page through the <a href="http://web.cvs.savannah.gnu.org/viewvc/www/philosophy/free-sw.html?root=www&amp;view=log">cvsweb interface</a>.</p>\r\n', '2017-07-23 11:29:07', '2017-07-23 11:29:07', 'GNU RichardStallman GNU/Linux FreeSoftware', 1, 1),
(4, 'Ubuntu wants to know which apps should be default in 18.04 LTS', '<p style="text-align: justify;">Ubuntu wants to know which apps Ubuntu YOU think it should include by default in its next LTS release, Ubuntu 18.04 LTS.</p>\r\n\r\n<p style="text-align: justify;">Having crowdsourced opinions on type of GNOME Shell experience you want, Ubuntu now seeks to comb through its community&rsquo;s collective softwares tastes to collate and curate a new set of default apps to include.</p>\r\n\r\n<p style="text-align: justify;">&ldquo;With the switch from Unity to GNOME, we&rsquo;re also reviewing some of the desktop applications we package and ship in Ubuntu. We&rsquo;re looking to crowdsource input on your favorite Linux applications across a broad set of classic desktop functionality,&rdquo; says Canonical&rsquo;s Dustin Kirkland.</p>\r\n\r\n<p style="text-align: justify;">And chances are most of you reading this will have an opinion or two to give!</p>\r\n\r\n<p style="text-align: justify;"></p>\r\n\r\n<p style="text-align: justify;">Which apps should Ubuntu include by default?<...', '<p style="text-align: justify;"><strong>Ubuntu wants to know which apps Ubuntu YOU think it should include by default in its next LTS release, Ubuntu 18.04 LTS.</strong></p>\r\n\r\n<p style="text-align: justify;">Having <a href="http://www.omgubuntu.co.uk/2017/06/ubuntu-reveal-results-gnome-desktop-survey" target="_blank">crowdsourced opinions on type of GNOME Shell experience</a> you want, Ubuntu now seeks to comb through its community&rsquo;s collective softwares tastes to collate and curate a new set of default apps to include.</p>\r\n\r\n<p style="text-align: justify;">&ldquo;With the switch from Unity to GNOME, we&rsquo;re also reviewing some of the desktop applications we package and ship in Ubuntu. We&rsquo;re looking to crowdsource input on your favorite Linux applications across a broad set of classic desktop functionality,&rdquo; <a href="http://blog.dustinkirkland.com/2017/07/ubuntu-1804-lts-desktop-default.html" onclick="_gaq.push([\'_trackEvent\', \'outbound-article\', \'http://blog.dustinkirkland.com/2017/07/ubuntu-1804-lts-desktop-default.html\', \'says\']);" target="_blank">says</a> <a href="https://www.linkedin.com/in/dustinkirkland/" onclick="_gaq.push([\'_trackEvent\', \'outbound-article\', \'https://www.linkedin.com/in/dustinkirkland/\', \'Canonicalâ€™s Dustin Kirkland.\']);" target="_blank">Canonical&rsquo;s Dustin Kirkland.</a></p>\r\n\r\n<p style="text-align: justify;">And chances are most of you reading this will have an opinion or two to give!</p>\r\n\r\n<p style="text-align: justify;"><a href="http://www.omgubuntu.co.uk/wp-content/uploads/2016/10/apps.jpg"><img alt="box of apps" src="http://www.omgubuntu.co.uk/wp-content/uploads/2016/10/apps-350x200.jpg" style="height:200px; width:350px" /></a></p>\r\n\r\n<p style="text-align: justify;">Which apps should Ubuntu include by default?</p>\r\n\r\n<p style="text-align: justify;">Are you keen for Chromium to become the stock web browser? Interested in having an IDE or IRC client included by default? Of the opinion that a different office suite ought to be offered?</p>\r\n\r\n<p style="text-align: justify;">Whatever your opinion this is your chance to<strong> let Ubuntu know</strong>.</p>\r\n\r\n<p style="text-align: justify;">&ldquo;Your feedback matters! There are hundreds of engineers and designers working for <em>you</em> to continue making Ubuntu amazing!&rdquo;</p>\r\n\r\n<h3 style="text-align: justify;">Copy &amp; Paste This List, Adding Your Preferences</h3>\r\n\r\n<p style="text-align: justify;">To share your opinion Kirkland recommends that you copy and paste the list below, but add in your preferred apps where applicable:</p>\r\n\r\n<ul>\r\n	<li style="text-align: justify;"><strong>Web Browser:</strong></li>\r\n	<li style="text-align: justify;"><strong>Email Client:</strong></li>\r\n	<li style="text-align: justify;"><strong>Terminal:</strong></li>\r\n	<li style="text-align: justify;"><strong>IDE:</strong></li>\r\n	<li style="text-align: justify;"><strong>File manager:</strong></li>\r\n	<li style="text-align: justify;"><strong>Basic Text Editor:</strong></li>\r\n	<li style="text-align: justify;"><strong>IRC/Messaging Client:</strong></li>\r\n	<li style="text-align: justify;"><strong>PDF Reader:</strong></li>\r\n	<li style="text-align: justify;"><strong>Office Suite:</strong></li>\r\n	<li style="text-align: justify;"><strong>Calendar:&nbsp;</strong></li>\r\n	<li style="text-align: justify;"><strong>Video Player:</strong></li>\r\n	<li style="text-align: justify;"><strong>Music Player:</strong></li>\r\n	<li style="text-align: justify;"><strong>Photo Viewer:</strong></li>\r\n	<li style="text-align: justify;"><strong>Screen recording:</strong></li>\r\n</ul>\r\n\r\n<p style="text-align: justify;">Notes</p>\r\n\r\n<p style="text-align: justify;"><strong>You can suggest multiple apps</strong> for each category but you must list them in order of preference (e.g., Web Browser: Chromium, Vivaldi, Lynx).</p>\r\n\r\n<p style="text-align: justify;"><strong>You can suggest non-free and non-open source software,</strong> but you must note this in your list (e.g,, Web Browser: Vivaldi, non-free)</p>\r\n\r\n<p style="text-align: justify;">If you no-longer use a desktop app for a given category, list what you do use. (E.g., Email Client: Gmail-web)</p>\r\n\r\n<p style="text-align: justify;">Finally if don&rsquo;t have&nbsp;a preference for a category (though I suspect most of you do) just skip it.</p>\r\n', '2017-07-23 11:35:13', '2017-07-23 11:35:13', 'Ubuntu UbuntuNews', 3, 1),
(5, 'SpaceX to Send Privately Crewed Dragon Spacecraft Beyond the Moon Next Year', '<p style="text-align: justify;">We are excited to announce that SpaceX has been approached to fly two private citizens on a trip around the Moon late next year. They have already paid a significant deposit to do a Moon mission. Like the Apollo astronauts before them, these individuals will travel into space carrying the hopes and dreams of all humankind, driven by the universal human spirit of exploration. We expect to conduct health and fitness tests, as well as begin initial training later this year. Other flight teams have also expressed strong interest and we expect more to follow. Additional information will be released about the flight teams, contingent upon their approval and confirmation of the health and fitness test results.</p>\r\n\r\n<p style="text-align: justify;">&nbsp;</p>\r\n\r\n<p style="text-align: justify;">Most importantly, we would like to thank NASA, without whom this would not be possible. NASA&rsquo;s Commercial Crew Program, which provided most of the funding for Drago...', '<p style="text-align: justify;">We are excited to announce that SpaceX has been approached to fly two private citizens on a trip around the Moon late next year. They have already paid a significant deposit to do a Moon mission. Like the Apollo astronauts before them, these individuals will travel into space carrying the hopes and dreams of all humankind, driven by the universal human spirit of exploration. We expect to conduct health and fitness tests, as well as begin initial training later this year. Other flight teams have also expressed strong interest and we expect more to follow. Additional information will be released about the flight teams, contingent upon their approval and confirmation of the health and fitness test results.</p>\r\n\r\n<p style="text-align: justify;">&nbsp;</p>\r\n\r\n<p style="text-align: justify;">Most importantly, we would like to thank NASA, without whom this would not be possible. NASA&rsquo;s Commercial Crew Program, which provided most of the funding for Dragon 2 development, is a key enabler for this mission. In addition, this will make use of the Falcon Heavy rocket, which was developed with internal SpaceX funding. Falcon Heavy is due to launch its first test flight this summer and, once successful, will be the most powerful vehicle to reach orbit after the Saturn V Moon rocket. At 5 million pounds of liftoff thrust, Falcon Heavy is two-thirds the thrust of Saturn V and more than double the thrust of the next largest launch vehicle currently flying.</p>\r\n\r\n<p style="text-align: justify;">&nbsp;</p>\r\n\r\n<p style="text-align: justify;">Later this year, as part of NASA&rsquo;s Commercial Crew Program, we will launch our Crew Dragon (Dragon Version 2) spacecraft to the International Space Station. This first demonstration mission will be in automatic mode, without people on board. A subsequent mission with crew is expected to fly in the second quarter of 2018. SpaceX is currently contracted to perform an average of four Dragon 2 missions to the ISS per year, three carrying cargo and one carrying crew. By also flying privately crewed missions, which NASA has encouraged, long-term costs to the government decline and more flight reliability history is gained, benefiting both government and private missions.</p>\r\n\r\n<p style="text-align: justify;">&nbsp;</p>\r\n\r\n<p style="text-align: justify;">Once operational Crew Dragon missions are underway for NASA, SpaceX will launch the private mission on a journey to circumnavigate the Moon and return to Earth. Lift-off will be from Kennedy Space Center&rsquo;s historic Pad 39A near Cape Canaveral &ndash; the same launch pad used by the Apollo program for its lunar missions. This presents an opportunity for humans to return to deep space for the first time in 45 years and they will travel faster and further into the Solar System than any before them.</p>\r\n\r\n<p style="text-align: justify;">Designed from the beginning to carry humans, the Dragon spacecraft already has a long flight heritage. These missions will build upon that heritage, extending it to deep space mission operations, an important milestone as we work towards our ultimate goal of transporting humans to Mars.</p>\r\n', '2017-07-23 11:39:40', '2017-07-23 11:39:40', 'SpaceX Dragon', 2, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `post_preview_image`
--

CREATE TABLE `post_preview_image` (
  `id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `post_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `post_preview_image`
--

INSERT INTO `post_preview_image` (`id`, `path`, `post_id`, `status`) VALUES
(1, '/web/userFiles/2/1500808636orbcommlanding.jpg', 1, 0),
(2, '/web/userFiles/2/1500808870spacex_barge.jpg', 1, 1),
(3, '/web/userFiles/2/1500809060crs11_water.jpg', 2, 1),
(4, '/web/userFiles/1/1500809347gnu_bumper.sh-600x600.png', 3, 1),
(5, '/web/userFiles/3/1500809713ubuntu1710-development-750x421.jpg', 4, 1),
(6, '/web/userFiles/2/1500809980dragonprivate_smallv2.jpg', 5, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `subscription`
--

CREATE TABLE `subscription` (
  `id` int(11) NOT NULL,
  `subscriber_id` int(11) NOT NULL,
  `target_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `subscription`
--

INSERT INTO `subscription` (`id`, `subscriber_id`, `target_id`, `created_at`, `updated_at`) VALUES
(1, 1, 2, '2017-07-23 11:25:03', '2017-07-23 11:25:03'),
(2, 3, 1, '2017-07-23 11:31:59', '2017-07-23 11:31:59'),
(3, 3, 2, '2017-07-23 11:32:04', '2017-07-23 11:32:04'),
(5, 2, 3, '2017-07-23 11:36:20', '2017-07-23 11:36:20'),
(6, 2, 1, '2017-07-23 11:36:23', '2017-07-23 11:36:23');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `first_name`, `last_name`, `email`, `password`, `status`, `created_at`) VALUES
(1, 'Yan', 'Kudriavtsev', 'yan@mail.com', '$2y$10$NRrDn1E2mtq1qg0aDLG/feG7gEhDB2Jd2nPWYkaeIafgVr9hnbRoe', 1, '2017-07-23 11:13:46'),
(2, 'Elon', 'Musk', 'elon@mail.com', '$2y$10$jZ8BX8aKuAs5NRMY.o4xNOrGgi9J9LVBWoe7I2H0ntE4dIsS3PF8K', 1, '2017-07-23 11:14:37'),
(3, 'Mark', 'Shuttleworth', 'mark@mail.com', '$2y$10$qwSkz49FehXvvDK9rLEKs.Ju0XXV9UUrpYqHnLw3rqV6om6z20xRq', 1, '2017-07-23 11:31:43');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `category_blog_post`
--
ALTER TABLE `category_blog_post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_blog_post_fk0` (`category_id`),
  ADD KEY `category_blog_post_fk1` (`post_id`);

--
-- Индексы таблицы `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comment_fk0` (`author_id`),
  ADD KEY `comment_fk1` (`post_id`),
  ADD KEY `comment_fk2` (`reply_to`);

--
-- Индексы таблицы `likes_of_posts`
--
ALTER TABLE `likes_of_posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `likes_of_posts_fk0` (`owner_id`),
  ADD KEY `likes_of_posts_fk1` (`post_id`);

--
-- Индексы таблицы `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notification_fk0` (`target_id`);

--
-- Индексы таблицы `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_fk0` (`author`);

--
-- Индексы таблицы `post_preview_image`
--
ALTER TABLE `post_preview_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`post_id`);

--
-- Индексы таблицы `subscription`
--
ALTER TABLE `subscription`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subscription_fk0` (`subscriber_id`),
  ADD KEY `subscription_fk1` (`target_id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `category_blog_post`
--
ALTER TABLE `category_blog_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT для таблицы `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `likes_of_posts`
--
ALTER TABLE `likes_of_posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `post_preview_image`
--
ALTER TABLE `post_preview_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `subscription`
--
ALTER TABLE `subscription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `category_blog_post`
--
ALTER TABLE `category_blog_post`
  ADD CONSTRAINT `category_blog_post_fk0` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `category_blog_post_fk1` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`);

--
-- Ограничения внешнего ключа таблицы `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `comment_fk0` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `comment_fk1` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`),
  ADD CONSTRAINT `comment_fk2` FOREIGN KEY (`reply_to`) REFERENCES `comment` (`id`);

--
-- Ограничения внешнего ключа таблицы `likes_of_posts`
--
ALTER TABLE `likes_of_posts`
  ADD CONSTRAINT `likes_of_posts_fk0` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `likes_of_posts_fk1` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`);

--
-- Ограничения внешнего ключа таблицы `notification`
--
ALTER TABLE `notification`
  ADD CONSTRAINT `notification_fk0` FOREIGN KEY (`target_id`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `post_fk0` FOREIGN KEY (`author`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `post_preview_image`
--
ALTER TABLE `post_preview_image`
  ADD CONSTRAINT `post_preview_image_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`);

--
-- Ограничения внешнего ключа таблицы `subscription`
--
ALTER TABLE `subscription`
  ADD CONSTRAINT `subscription_fk0` FOREIGN KEY (`subscriber_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `subscription_fk1` FOREIGN KEY (`target_id`) REFERENCES `user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
