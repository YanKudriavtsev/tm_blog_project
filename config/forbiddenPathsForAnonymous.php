<?php

return [
    '~/post/add~',
    '~/post/edit~',
    '~/post/delete~',
    '~/post/user-posts/admin~',
    '~/comment/add~',
    '~/user/delete~',
    '~/user/edit~',
    '~/user/subscribe/([0-9]+)~',
    '~/user/unsubscribe/([0-9]+)~',
    '~/user/subscriptions~',
    '~/notification/delete/([0-9]+)~',
    '~/like/add/([0-9]+)~'
];