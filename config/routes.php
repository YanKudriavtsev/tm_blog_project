<?php
return [
    // post
    '~^/posts$~' => 'PostController/postsAction',
    '~^/posts/tag/(.+)$~' => 'PostController/postsByTagAction/$1',
    '~^/post/([0-9]+)$~' => 'PostController/getAction/$1',
    '~^/post/add$~' => 'PostController/addAction',
    '~^/post/edit/([0-9]+)$~' => 'PostController/editAction/$1',
    '~^/post/delete/([0-9]+)$~' => 'PostController/deleteAction/$1',
    '~^/post/user-posts/([0-9]+)$~' => 'PostController/userPostsAction/$1',
    '~^/post/user-posts/admin$~' => 'PostController/userPostsAdminAction',

    // category
    '~^/category/([0-9]+)$~' => 'PostController/getByCategoryIdAction/$1',

    // comment
    '~^/comment/add$~' => 'CommentController/addAction',

    // user
    '~^/user/registration$~' => 'UserController/registerAction',
    '~^/user/login$~' => 'UserController/loginAction',
    '~^/user/logout$~' => 'UserController/logoutAction',
    '~^/user/edit$~' => 'UserController/editAction',

    // subscriptions
    '~^/user/subscribe/([0-9]+)$~' => 'SubscriptionController/subscribeAction/$1',
    '~^/user/unsubscribe/([0-9]+)$~' => 'SubscriptionController/unsubscribeAction/$1',
    '~^/user/subscriptions$~' => 'SubscriptionController/subscriptionsAction',

    // search
    '~^/search$~' => 'SearchController/searchAction',

    // error
    '~^/error/show/([0-9]+)$~' => 'ErrorController/showAction/$1',

    //notification
    '~^/notification/delete/([0-9]+)$~' => 'NotificationController/deleteAction/$1',

    // likes
    '~^/like/add/([0-9]+)$~' => 'LikeController/addAction/$1',

    // root
    '~^/$~' => 'PostController/postsAction',
];