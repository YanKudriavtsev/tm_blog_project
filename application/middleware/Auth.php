<?php
use Framework\MiddlewareInterface;
use Framework\Controller;

class Auth implements MiddlewareInterface
{
    public static function run()
    {
        $url = UrlHelper::getUrl();
        $forbiddenPathsForAnonymous = require_once ROOT . '/config/forbiddenPathsForAnonymous.php';

        /*
         * If array $_SESSION don`t have key 'user' and requested URL
         * is in array $forbiddenPathsForAnonymous - then request be redirected to login page
         */
        if ((!AuthHelper::userIsAuthenticated())) {
            foreach ($forbiddenPathsForAnonymous as $path) {
                if (preg_match($path, $url)) {
                    $frameworkController = new Controller();
                    $frameworkController->redirect('/user/login');
                }
            }
        }
    }
}
