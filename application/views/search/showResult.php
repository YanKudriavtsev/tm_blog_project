<h1 class="page-header">
    <?=$pageHeader;?>
</h1>

<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#posts">Posts</a></li>
    <li><a data-toggle="tab" href="#tags">Tags</a></li>
    <li><a data-toggle="tab" href="#users">Users</a></li>
</ul>

<div class="tab-content">
    <!-- Result of post search -->
    <div id="posts" class="tab-pane fade in active">
        <h3>Posts</h3>
        <hr>
        <?php foreach ($postsResult as $post): ?>
            <a href="/post/<?=$post['id'];?>"><span><?=$post['title'];?></span></a>
            <hr>
        <?php endforeach;?>
    </div>

    <!-- Result of tags search -->
    <div id="tags" class="tab-pane fade">
        <h3>Tags</h3>
        <hr>
        <?php foreach ($tagsResult as $post): ?>
            <a href="/post/<?=$post['id'];?>"><span><?=$post['title'];?></span></a>
            <hr>
        <?php endforeach;?>
    </div>

    <!-- Result of user search -->
    <div id="users" class="tab-pane fade">
        <h3>Users</h3>
        <hr>
        <?php foreach ($usersResult as $user): ?>
            <a href="/post/user-posts/<?=$user['id'];?>">
                <?=$user['first_name'];?> <?=$user['last_name'];?>
            </a>
            <br>
            <span><?=$user['email'];?></span>
            <hr>
        <?php endforeach;?>
    </div>
</div>