<div class="col-md-12">
    <form action="/post/edit/<?=$post['id'];?>" method="post" enctype="multipart/form-data">
        <!-- Hiden field with post id -->
        <input type="text" value="<?=$post['id'];?>" hidden name="postId">

        <!-- Hidden field with author id -->
        <input type="text" name="postAuthorId" hidden value="<?=AuthHelper::getAuthenticatedUser()['id'];?>">

        <!-- Post title -->
        <label>Title: </label>
        <input type="text" name="postTitle" class="form-control" required value="<?=$post['title'];?>">

        <!-- Post text -->
        <label>Text: </label>
        <textarea name="postText" class="form-control"><?=$post['text'];?></textarea>
        <hr>

        <!-- Prewiev image -->
        <label>Preview image</label>
        <input type="file" name="postPicture">
        <hr>

        <!-- Post categories -->
        <label>Post categories:</label><br>
        <?php foreach (Category::getAll() as $category):?>
            <label class="checkbox-inline">
                <!-- Checked box if the category is the blog category -->
                <?php $flag = in_array($category['id'], $idOfPostCategories) ? 'checked': '';?>
                <input type="checkbox" name="postCategory[]" <?=$flag;?> value="<?=$category['id'];?>">
                <?=$category['name'];?>
            </label>
        <?php endforeach;?>
        <hr>

        <!-- Post tags -->
        <label class="form-control">Tags:</label>
        <textarea class="form-control" name="tags"><?=$post['tags'];?></textarea>
        <hr>

        <!-- Submit button -->
        <button type="submit" name="submit" class="btn btn-default form-control">Submit</button>
    </form>
</div>