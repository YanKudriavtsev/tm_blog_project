<h1 class="page-header">
    <?=$pageHeader;?>
</h1>
<?php foreach($posts as $post) :?>
    <div class="post-admin-block">
        <div class="row">

            <!-- Post title -->
            <div class="col-lg-7">
                <a href="/post/<?=$post['id'];?>">
                    <span><?=$post['title'];?></span>
                </a>
            </div>

            <!-- Created date of post -->
            <div class="col-lg-3">
                <?=$post['created_at'];?>
            </div>

            <!-- Edit icon -->
            <div class="col-lg-1">
                <a href="/post/edit/<?=$post['id'];?>">
                    <span class="glyphicon glyphicon-pencil"></span>
                </a>
            </div>

            <!-- Remove icon -->
            <div class="col-lg-1">
                <a href="/post/delete/<?=$post['id'];?>">
                    <span class="glyphicon glyphicon-remove"></span>
                </a>
            </div>
        </div>
    </div>
<?php endforeach;?>

<!-- Pager -->
<?php if($posts):?>
    <ul class="pager">
        <!-- Do not show "Newer" button if page = 1 -->
        <?php if($page > 1):?>
            <li class="previous">
                <a href="/posts?page=<?=$page-1;?>">&larr; Newer</a>
            </li>
        <?php endif;?>

        <li class="next">
            <a href="/posts?page=<?=$page+1;?>">Older &rarr;</a>
        </li>
    </ul>
<?php endif;?>