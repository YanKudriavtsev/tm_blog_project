<div class="col-md-12">
    <form action="/post/add" method="post" enctype="multipart/form-data">

        <!-- Hidden field with user id -->
        <input type="text" name="postAuthorId" hidden value="<?=AuthHelper::getAuthenticatedUser()['id'];?>">
        
        <!-- Title field -->
        <label>Title: </label>
        <input type="text" name="postTitle" class="form-control" required>

        <!-- Content field -->
        <label>Text: </label>
        <textarea name="postText" class="form-control"></textarea>
        <hr>

        <!-- Prewiev image -->
        <label>Preview image</label>
        <input type="file" name="postPicture">
        <hr>

        <!-- Categories -->
        <label>Post categories:</label><br>
        <?php foreach (Category::getAll() as $category):?>
            <label class="checkbox-inline">
                <input type="checkbox" name="postCategory[]" value="<?=$category['id'];?>">
                <?=$category['name'];?>
            </label>
        <?php endforeach;?>
        <hr>

        <!-- Tags -->
        <label class="form-control">Tags:</label>
        <textarea class="form-control" name="tags"></textarea>
        <hr>

        <!-- Sybmit button -->
        <button type="submit" name="submit" class="btn btn-default form-control">Submit</button>
    </form>
</div>