<h1 class="page-header">
    <?=$pageHeader;?>
</h1>
<?php if($newestPosts):?>
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#newestPosts">Newest posts</a></li>
        <li><a data-toggle="tab" href="#mostPopularPosts">Most popular posts</a></li>
    </ul>
    <div class="tab-content">
        <!-- Newest Posts -->
        <div id="newestPosts" class="tab-pane fade in active">
            <h3>Newest posts</h3>
            <hr>
            <?php foreach($newestPosts as $post) :?>
                <div class="post-preview">

                    <!-- Post title -->
                    <h2><a href="/post/<?=$post['id'];?>"><?=$post['title'];?></a></h2>

                    <!-- Post author -->
                    <p class="lead">
                        by <a href="/post/user-posts/<?=$post['author'];?>">
                            <?=$post['first_name'] . ' ' . $post['last_name'];?>
                        </a>

                        <!-- Subscription button -->
                        <?php if(SubscriptionsHelper::userIsSubscribed($post['author'])): ?>
                            <a href="/user/unsubscribe/<?=$post['author'];?>">
                                <button class="btn btn-default btn-xs">unsubscribe</button>
                            </a>
                        <?php else:?>
                            <a href="/user/subscribe/<?=$post['author'];?>">
                                <button class="btn btn-default btn-xs">subscribe</button>
                            </a>
                        <?php endif;?>

                    </p>

                    <!-- Date of post -->
                    <p>
                        <span class="glyphicon glyphicon-time"></span> Posted on <?=$post['created_at'];?>
                    </p>

                    <!-- Short content of post -->
                    <p><img src="<?=$post['preview_image'];?>"><?=$post['short_content'];?></p>
                    <span class="like-block">
                        <span class="glyphicon glyphicon-thumbs-up liked-icon"></span>
                        <?=$post['likes'];?>
                    </span><br>

                    <!--"Read more" button-->
                    <a class="btn btn-primary" href="/post/<?=$post['id'];?>">
                        Read More <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                    <hr>

                </div>
                <!-- /.post-preview -->
            <?php endforeach;?>
        </div>

        <!-- Most popular posts -->
        <div id="mostPopularPosts" class="tab-pane fade">
            <h3>Most popular posts</h3>
            <hr>
            <?php foreach($mostPopularPosts as $post) :?>
                <div class="post-preview">

                    <!-- Post title -->
                    <h2><a href="/post/<?=$post['id'];?>"><?=$post['title'];?></a></h2>

                    <!-- Post author -->
                    <p class="lead">
                        by <a href="/post/user-posts/<?=$post['author'];?>">
                            <?=$post['first_name'] . ' ' . $post['last_name'];?>
                        </a>

                        <!-- Subscription button -->
                        <?php if(SubscriptionsHelper::userIsSubscribed($post['author'])): ?>
                            <a href="/user/unsubscribe/<?=$post['author'];?>">
                                <button class="btn btn-default btn-xs">unsubscribe</button>
                            </a>
                        <?php else:?>
                            <a href="/user/subscribe/<?=$post['author'];?>">
                                <button class="btn btn-default btn-xs">subscribe</button>
                            </a>
                        <?php endif;?>

                    </p>

                    <!-- Date of post -->
                    <p>
                        <span class="glyphicon glyphicon-time"></span> Posted on <?=$post['created_at'];?>
                    </p>

                    <!-- Short content of post -->
                    <p><img src="<?=$post['preview_image'];?>"><?=$post['short_content'];?></p>
                    <span class="like-block">
                        <span class="glyphicon glyphicon-thumbs-up liked-icon"></span>
                        <?=$post['likes'];?>
                    </span><br>

                    <!--"Read more" button-->
                    <a class="btn btn-primary" href="/post/<?=$post['id'];?>">
                        Read More <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                    <hr>

                </div>
                <!-- /.post-preview -->
            <?php endforeach;?>
        </div>
    </div>

    <?php if($newestPosts or $mostPopularPosts):?>
        <!-- Pager -->
        <ul class="pager">
            <!-- Do not show "Newer" button if page = 1 -->
            <?php if($page > 1):?>
                <li class="previous">
                    <a href="?page=<?=$page-1;?>">&larr; Newer</a>
                </li>
            <?php endif;?>

            <li class="next">
                <a href="?page=<?=$page+1;?>">Older &rarr;</a>
            </li>
        </ul>
    <?php endif;?>
<?php else:?>
    <h1>No posts yet :( Get <a href="<?=$_SERVER['HTTP_REFERER'];?>">back</a></h1>
<?php endif;?>
