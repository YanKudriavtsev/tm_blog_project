<!-- Post title -->
<h1><?=$post['title'];?></h1>

<!-- Author -->
<p class="lead">
    by <a href="/post/user-posts/<?=$post['author'];?>"><?=$post['first_name'] . ' ' . $post['last_name'];?></a>

    <!-- Subscription button -->
    <?php if(SubscriptionsHelper::userIsSubscribed($post['author'])): ?>
        <a href="/user/unsubscribe/<?=$post['author'];?>">
            <button class="btn btn-default btn-xs">unsubscribe</button>
        </a>
    <?php else:?>
        <a href="/user/subscribe/<?=$post['author'];?>">
            <button class="btn btn-default btn-xs">subscribe</button>
        </a>
    <?php endif;?>
</p>
<hr>

<!-- Date/Time -->
<p><span class="glyphicon glyphicon-time"></span> Posted on <?=$post['created_at'];?></p>
<hr>

<!-- Post Content -->
<p>
    <?=$post['text'];?><?=$post['tags'];?>
    <span class="like-block">
        <a href="/like/add/<?=$post['id'];?>">
                <span class="glyphicon glyphicon-thumbs-up <?=$likeIconClass;?>"></span>
        </a>
        <?=$post['likes'];?>
    </span>
</p>

<!-- Post Categories -->
<?php if ($postCategories):?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Post categories</h3>
        </div>
        <div class="panel-body">
            <?php foreach($postCategories as $postCategory) :?>
                <a href="/category/<?=$postCategory['id'];?>"><?=$postCategory['name'];?></a>
            <?php endforeach;?>
        </div>
    </div>
<?php endif;?>

<!-- Blog Comments -->

<!-- Comments Form -->
<div class="well">
    <h4>Leave a Comment:</h4>
    <form role="form" action="/comment/add" method="post" id="comment-post">
        <input type="text" hidden value="<?=$post['id'];?>" name="postId">
        <input type="text" hidden value="" name="replyCommentId" id="replyCommentIdInput">
        <input type="text" hidden value="<?=AuthHelper::getAuthenticatedUser()['id'];?>" name="commentAuthorId">
        <div class="form-group">
            <textarea class="form-control" name="commentText"></textarea>
        </div>
        <button type="submit" class="btn btn-primary" name="submit">Submit</button>
    </form>
</div>
<hr>

<!-- Posted Comments -->
<h3>Comments:</h3>
<?php if ($comments): ?>
    <?php foreach ($comments as $comment_id => $comment):?>

                <!-- Anchor link -->
        <a name="<?=$comment_id;?>"></a>

        <div class="media">
            <div class="media-body">
                <h4 class="media-heading">
                    <a href="/user/edit/<?=$comment[0]['author_id'];?>">
                        <!-- Comment author -->
                        <?=$comment[0]['first_name'] . ' ' . $comment[0]['last_name'];?>
                    </a>
                    <!-- Date of comment -->
                    <small><?=$comment[0]['created_at'];?></small>
                </h4>
                <p>
                    <?php if($comment[0]['reply_to']):?>

                        <!-- Reference to the comment on which gave the answer -->
                        <a href="#<?=$comment[0]['reply_to'];?>">
                            <?=$comments[$comment[0]['reply_to']][0]['first_name'];?> <?=$comments[$comment[0]['reply_to']][0]['last_name'];?>
                        </a>,
                    <?php endif;?>
                    <!-- Text of comment -->
                    <?=$comment[0]['text'];?>
                </p>
                <button class="reply-to-comment" comment-id="<?=$comment_id;?>">reply</button>
            </div>
        </div>
    <?php endforeach;?>
<?php else:?>
    <p>No comments yet :(</p>
<?php endif;?>
