<h1 class="page-header">
        <?=$pageHeader;?>
</h1>

<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#subscriptions">Subscriptions</a></li>
    <li>
        <a data-toggle="tab" href="#notifications">
            Notifications
            (<?=NotificationHelper::getNumberOfNotificationForUser();?>)
        </a>
    </li>
</ul>

<div class="tab-content">
    <!-- User subscriptions block -->
    <div id="subscriptions" class="tab-pane fade in active">
        <h3>Subscriptions</h3>
        <hr>
        <?php foreach ($userSubscriptions as $user): ?>
            <div class="row">
                <!-- User -->
                <div class="col-lg-3">
                    <a href="/post/user-posts/<?=$user['id'];?>">
                        <?=$user['first_name'];?> <?=$user['last_name'];?>
                    </a>
                </div>

                <!-- Email -->
                <div class="col-lg-4">
                    <span><?=$user['email'];?></span>
                </div>

                <!-- Remove icon -->
                <div class="col-lg-1 col-lg-offset-4">
                    <a href="/user/unsubscribe/<?=$user['id'];?>">
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>
                </div>
            </div>
            <hr>
        <?php endforeach;?>
    </div>

    <!-- User notifications block -->
    <div id="notifications" class="tab-pane fade">
        <h3>Notifications</h3>
        <hr>
        <?php foreach ($userNotifications as $notification): ?>
            <div class="row">
                <!-- Text of notification -->
                <div class="col-lg-11">
                    <?=$notification['text'];?>
                </div>

                <!-- Remove icon -->
                <div class="col-lg-1">
                    <a href="/notification/delete/<?=$notification['id'];?>">
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>
                </div>
            </div>
            <hr>
        <?php endforeach;?>
    </div>
</div>