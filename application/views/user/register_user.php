<div class="row">
    <div class="col-lg-6 col-lg-offset-3">
        <!-- Block of showing errors -->
        <?php if(isset($errors)): ?>
            <div class="errors">
                <?php foreach ($errors as $error) : ?>
                    <div class="alert alert-danger">
                        <?=$error?>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>

        <!-- Registration form -->
        <form action="/user/registration" method="post">
            <!-- First name -->
            <div class="form-group">
                <label>First name</label>
                <input type="text" class="form-control" name="user[first_name]" value="<?=$inputData['first_name'];?>"  placeholder="First name">
            </div>

            <!-- Last name -->
            <div class="form-group">
                <label>Last name</label>
                <input type="text" class="form-control" name="user[last_name]" value="<?=$inputData['last_name'];?>" placeholder="Last name">
            </div>

            <!-- Email -->
            <div class="form-group">
                <label>Email</label>
                <input type="text" class="form-control" name="user[email]" value="<?=$inputData['email'];?>" placeholder="Email">
            </div>

            <!-- Password -->
            <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control" name="user[password]" value="" placeholder="Password">
            </div>

            <!-- Confirmed password -->
            <div class="form-group">
                <label>Confirm password</label>
                <input type="password" class="form-control" name="user[confirm_password]" value="" placeholder="Confirm password">
            </div>

            <!-- Submit button -->
            <button type="submit" class="btn btn-default form-control">Submit</button>
        </form>
    </div>
</div>