<div class="row">
    <div class="col-lg-6 col-lg-offset-3">
        <form action="/user/edit/<?=$user['id']?>" method="post">
            <!-- First name -->
            <div class="form-group">
                <label for="exampleInputEmail1">Name</label>
                <input type="text" class="form-control" name="user[first_name]" value="<?=$user['first_name']?>"  placeholder="Name">
            </div>

            <!-- Last name -->
            <div class="form-group">
                <label for="exampleInputPassword1">Last name</label>
                <input type="text" class="form-control" name="user[last_name]" value="<?=$user['last_name']?>" placeholder="Password">
            </div>

            <!-- Email -->
            <div class="form-group">
                <label for="exampleInputPassword1">Email</label>
                <input type="text" class="form-control" name="user[email]" value="<?=$user['email']?>" placeholder="Email">
            </div>

            <!-- Date of registration -->
            <div class="form-group">
                <label for="exampleInputPassword1">Registration date</label>
                <input type="text" class="form-control" name="user[birthday]" value="<?=$user['created_at']?>" placeholder="Birth date" disabled>
            </div>

            <!-- Submit button -->
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>
</div>
