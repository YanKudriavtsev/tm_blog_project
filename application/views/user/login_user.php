<div class="row">
    <div class="col-lg-6 col-lg-offset-3">
        <!-- Block of showing errors -->
        <?php if(isset($errors)): ?>
            <div class="errors">
                <?php foreach ($errors as $error) : ?>
                    <div class="alert alert-danger">
                        <?=$error?>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>

        <!-- Login form -->
        <form action="/user/login" method="post">
            <!-- Email -->
            <div class="form-group">
                <label for="exampleInputEmail1">Email</label>
                <input type="text" class="form-control" name="user[email]" value=""  placeholder="Email">
            </div>

            <!-- Passowrd -->
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" name="user[password]" value="" placeholder="Password">
            </div>

            <!-- Submit button -->
            <button type="submit" class="btn btn-default form-control">Submit</button>
        </form>
    </div>
</div>
