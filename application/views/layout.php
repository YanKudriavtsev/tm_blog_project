<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php if(isset($pageHeader)):?>
        <title><?=$pageHeader;?></title>
    <?php else:?>
        <title>Bloggy</title>
    <?php endif;?>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="/web/css/bootstrap.min.css">
    <!-- Bootstrap optional theme -->
    <link rel="stylesheet" href="/web/css/bootstrap-theme.min.css">
    <!-- Main CSS -->
    <link rel="stylesheet" href="/web/css/main.css">
</head>
<body>
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">Bloggy</a>
            </div>

            <!-- User menu -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <?php if (AuthHelper::userIsAuthenticated()):?>
                        <!-- Create post -->
                        <li>
                            <a href="/post/add">Create post</a>
                        </li>

                        <!-- Show user posts -->
                        <li>
                            <a href="/post/user-posts/admin">My posts</a>
                        </li>

                        <!-- Show user subscriptions -->
                        <li>
                            <a href="/user/subscriptions">
                                My subscriptions
                                (<?=NotificationHelper::getNumberOfNotificationForUser();?>)
                            </a>
                        </li>
                    <?php endif;?>
                </ul>

                <!-- Login/logout navigation -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authenticated user -->
                    <?php if (AuthHelper::userIsAuthenticated()):?>
                        <!-- Name of user -->
                        <li>
                            <a href="/user/edit">
                                <?=AuthHelper::getAuthenticatedUser()['first_name'];?> <?=AuthHelper::getAuthenticatedUser()['last_name'];?>
                            </a>
                        </li>

                        <!-- Logout -->
                        <li>
                            <a href="/user/logout">Logout</a>
                        </li>

                    <!-- Non authenticated user -->
                    <?php else:?>
                        <!-- Login -->
                        <li>
                            <a href="/user/login">Login</a>
                        </li>

                        <!-- Registration -->
                        <li>
                            <a href="/user/registration">Registration</a>
                        </li>
                    <?php endif;?>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <!-- Blog content -->
            <div class="col-md-9">
                <?php require_once $content_page; ?>
            </div>

            <!-- Blog Sidebar Widgets Column -->
            <div class="col-md-3">

                <!-- Blog Search Well -->
                <div class="well">
                    <h4>Search</h4>
                        <!-- Serach form -->
                        <form action="/search" method="POST"">
                            <div class="input-group">
                                <!-- Search input string -->
                                <input type="text" class="form-control" name="q" required>

                                <!-- Submit button -->
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </button>
                                </span>
                            </div>
                        </form>
                </div>

                <!-- Blog Categories -->
                <div class="well">
                    <h4>Categories</h4>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="categories-sidebar">
                                <ul class="list-unstyled">
                                    <?php foreach (Category::getAll() as $category):?>
                                        <a href="/category/<?=$category['id'];?>">
                                            <li><?=$category['name'];?></li>
                                        </a>
                                    <?php endforeach;?>
                                </ul>
                            </div>
                            <!-- /.categories-sidebar -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
            </div>

        </div>
        <!-- /.row -->
        <hr>
        <!-- Footer -->
        <footer>
            <!-- Copyright -->
            <div class="row">
                <div class="col-lg-12">
                    <p class="text-center">Copyright &copy; Yan Kudriavtsev <?=date('Y');?></p>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- JS -->
    <script src="/web/js/jquery-3.2.1.min.js"></script>
    <script src="/web/js/bootstrap.min.js"></script>
    <script src="/web/ckeditor/ckeditor.js"></script>
    <script>
        $(".reply-to-comment").click(function () {
            var reply_comment_id = this.getAttribute('comment-id');
            $("#replyCommentIdInput").attr("value", reply_comment_id);
            $('html, body').animate({
                scrollTop: $("#comment-post").offset().top - 150
            }, 700);
        });
    </script>
    <script>
        CKEDITOR.replace('postText');
    </script>

</body>
</html>
