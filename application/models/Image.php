<?php
use \Framework\Model;

class Image extends Model
{
    const TABLE_NAME = 'post_preview_image';

    public static function save($imagePath, $postId)
    {
        $sql = "INSERT INTO ".self::TABLE_NAME.
               " (path, post_id)".
               " VALUES (?, ?)";

        $query = self::getConnection()->prepare($sql);
        $query->execute(
            [
                $imagePath,
                $postId
            ]
        );
    }

    public static function deletePreviewImage($postId)
    {
        $sql = "UPDATE ".self::TABLE_NAME.
               " SET status = 0".
               " WHERE post_id = ?";

        $query = self::getConnection()->prepare($sql);
        $query->execute([$postId]);
    }
}
