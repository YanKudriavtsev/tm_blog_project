<?php
use Framework\Model;

class Category extends Model
{
    const TABLE_NAME = 'category';

    public static function getAll()
    {
        $sql = "SELECT id, name FROM ".self::TABLE_NAME." ORDER BY name";

        $query = self::getConnection()->prepare($sql);
        $query->execute([]);

        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    public static function getById($categoryId)
    {
        $sql = "SELECT name FROM ".self::TABLE_NAME." WHERE id=?";

        $query = self::getConnection()->prepare($sql);
        $query->execute([$categoryId]);

        $result = $query->fetch(PDO::FETCH_ASSOC);

        return $result;
    }

    public static function addPostToCategory($categoriesIdList, $postId)
    {
        /* 
        * Creates string for inserting many rows to database
        * INSERT INTO table (field1, field2) VALUES (1, 2), (3, 4), (5, 6)
        */
        foreach ($categoriesIdList as $key => $id) {
            // creates string (1, 2) and added him to array
            $categoriesIdList[$key] = "({$id}, {$postId})";
        }
        // Implods array to final string
        $categoriesIdList = implode(',', $categoriesIdList);

        $sql = "INSERT INTO category_blog_post".
               " (category_id, post_id)".
               " VALUES {$categoriesIdList}";

        $query = self::getConnection()->prepare($sql);
        $query->execute([]);
    }

    public static function getCategoriesByPostId($postId)
    {
        $sql = "SELECT c.id, c.name FROM ".self::TABLE_NAME." AS c ".
               "INNER JOIN category_blog_post AS cbp ON cbp.post_id = ? ".
               "WHERE c.id = cbp.category_id";

        $query = self::getConnection()->prepare($sql);
        $query->execute([$postId]);

        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    public static function deletePostFromCategories($postId)
    {
        $sql = "DELETE FROM category_blog_post WHERE post_id = ?";

        $query = self::getConnection()->prepare($sql);
        $query->execute([$postId]);
    }
}