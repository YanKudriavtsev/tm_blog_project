<?php
use Framework\Model;

class Post extends Model
{
    const TABLE_NAME = 'post';
    const NUMBER_OF_POST_PER_PAGE = POST_PER_PAGE;

    public static function getAll($page=1, $fieldForSort)
    {
        $offset = ($page - 1) * self::NUMBER_OF_POST_PER_PAGE;
        $sql = "SELECT p.*, user.first_name, user.last_name, COUNT(l.id) AS likes, i.path AS preview_image ".
               "FROM ".self::TABLE_NAME." AS p ".

               // first name and last name post author
               "INNER JOIN user ON p.author = user.id ".

               // preview image for post
               "INNER JOIN post_preview_image AS i ON p.id = i.post_id ".

               // post likes
               "LEFT JOIN likes_of_posts AS l ON p.id = l.post_id ".
               "WHERE p.status = 1 AND i.status = 1 ".
               "GROUP BY p.id, i.path ".
               "ORDER BY {$fieldForSort} DESC ".
               "LIMIT ".self::NUMBER_OF_POST_PER_PAGE.
               " OFFSET ".$offset;

        $query = self::getConnection()->prepare($sql);
        $query->execute([]);
        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    public static function getById($postId)
    {
        $sql = "SELECT p.*, u.first_name, u.last_name, COUNT(l.id) AS likes ".
               "FROM ".self::TABLE_NAME." AS p ".

               // first name and last name post author
               "INNER JOIN user AS u ON p.author = u.id ".

               // post likes
               "INNER JOIN likes_of_posts AS l ON p.id = l.post_id ".
               "WHERE p.id=? AND p.status=1";

        $query = self::getConnection()->prepare($sql);
        $query->execute(
            [
                $postId
            ]
        );

        $result = $query->fetch(PDO::FETCH_ASSOC);

        return $result;
    }

    public static function getByAuthorId($userId, $page=1, $fieldForSort)
    {
        $offset = ($page - 1) * self::NUMBER_OF_POST_PER_PAGE;
        $sql = "SELECT p.*, u.first_name, u.last_name, COUNT(l.id) AS likes, i.path AS preview_image ".
               "FROM ".self::TABLE_NAME." AS p ".

               // first name and last name post author
               "INNER JOIN user AS u ON p.author = u.id ".

               // preview image for post
               "INNER JOIN post_preview_image AS i ON p.id = i.post_id ".

               // post likes
               "LEFT JOIN likes_of_posts AS l ON p.id = l.post_id ".
               "WHERE p.author=? AND p.status=1 AND i.status = 1 ".
               "GROUP BY p.id, i.path ".
               "ORDER BY {$fieldForSort} DESC ".
               "LIMIT ".self::NUMBER_OF_POST_PER_PAGE.
               " OFFSET ".$offset;

        $query = self::getConnection()->prepare($sql);
        $query->execute(
            [
                $userId,
            ]
        );
        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    public static function getByCategoryId($categoryId, $page=1, $fieldForSort)
    {
        $offset = ($page - 1) * self::NUMBER_OF_POST_PER_PAGE;
        $sql = "SELECT p.*, u.first_name, u.last_name, COUNT(l.id) AS likes, i.path AS preview_image ".
               "FROM ".self::TABLE_NAME." AS p ".
               "INNER JOIN category_blog_post AS cbp ON p.id=cbp.post_id AND cbp.category_id=? ".

               // first name and last name post author
               "INNER JOIN user as u ON u.id=p.author ".

               // preview image for post
               "INNER JOIN post_preview_image AS i ON p.id = i.post_id ".

               // post likes
               "LEFT JOIN likes_of_posts AS l ON p.id = l.post_id ".
               "WHERE p.status=1 AND i.status = 1 ".
               "GROUP BY p.id, i.path ".
               "ORDER BY {$fieldForSort} DESC ".
               "LIMIT ".self::NUMBER_OF_POST_PER_PAGE.
               " OFFSET ".$offset;

        $query = self::getConnection()->prepare($sql);
        $query->execute(
            [
                $categoryId,
            ]
        );

        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    public static function deleteById($postId)
    {
        $sql = "UPDATE ".self::TABLE_NAME.
               " SET status = 0 WHERE id = ?";
        $query = self::getConnection()->prepare($sql);
        $query->execute([$postId]);
    }

    public static function update($data)
    {
        $shortContent = substr(
            strip_tags(
                $data['postText'],
                '<p>'),
            0,
            1000) . '...';

        $sql = "UPDATE ".self::TABLE_NAME.
               " SET title = ?, text = ?, updated_at = ?, short_content = ?, tags = ?".
               " WHERE id=?";

        $query = self::getConnection()->prepare($sql);

        $query->execute([
            $data['postTitle'],
            $data['postText'],
            date("Y-m-d H:i:s"),
            $shortContent,
            str_replace('#', '', $data['tags']),
            $data['postId']
        ]);
    }

    public static function save($data)
    {
        $shortContent = substr(
            strip_tags(
                $data['postText'],
                '<p>'),
            0,
            1000) . '...';

        $sql = "INSERT INTO ".self::TABLE_NAME.
               " (title, short_content, text, tags, author, created_at)".
               " VALUES (?, ?, ?, ?, ?, ?)";

        $query = self::getConnection()->prepare($sql);
        $query->execute([
            $data['postTitle'],
            $shortContent,
            $data['postText'],
            str_replace('#', '', $data['tags']),
            $data['postAuthorId'],
            date("Y-m-d H:i:s")
        ]);

        $postId = self::getConnection()->lastInsertId();
        return $postId;
    }

    public static function getByTag($tag, $page=2, $fieldForSort)
    {
        $offset = ($page - 1) * self::NUMBER_OF_POST_PER_PAGE;
        $sql = "SELECT COUNT(p.id) AS count, p.*, user.first_name, user.last_name, COUNT(l.id) AS likes, i.path AS preview_image ".
               "FROM ".self::TABLE_NAME." AS p ".

               // first name and last name post author
               "INNER JOIN user ON p.author = user.id ".

               // preview image for post
               "INNER JOIN post_preview_image AS i ON p.id = i.post_id ".

               // post likes
               "LEFT JOIN likes_of_posts AS l ON p.id = l.post_id ".
               "WHERE p.status = 1 AND tags LIKE ? AND i.status = 1 ".
               "GROUP BY p.id, i.path ".
               "ORDER BY {$fieldForSort} DESC ".
               "LIMIT ".self::NUMBER_OF_POST_PER_PAGE.
               " OFFSET ".$offset;
        $query = self::getConnection()->prepare($sql);
        $query->execute(
            [
                "%{$tag}%",
            ]
        );

        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }
}