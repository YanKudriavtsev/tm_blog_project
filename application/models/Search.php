<?php
use \Framework\Model;

class Search extends Model
{
    public static function userSearch($q)
    {
        $sql = "SELECT id, first_name, last_name, email FROM user ".
               "WHERE CONCAT(first_name, ' ', last_name) LIKE :q";

        $query = self::getConnection()->prepare($sql);
        $query->execute(
            [
                ':q' => "%{$q}%",
            ]
        );

        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    public static function tagSearch($q)
    {
        $sql = "SELECT id, title, short_content FROM post ".
               "WHERE tags LIKE ? AND status = 1";

        $query = self::getConnection()->prepare($sql);
        $query->execute(
            [
                "%{$q}%",
            ]
        );

        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    public static function postSearch($q)
    {
        $sql = "SELECT id, title, short_content FROM post ".
               "WHERE title LIKE :q OR text LIKE :q AND status = 1";

        $query = self::getConnection()->prepare($sql);
        $query->execute(
            [
                ':q' => "%{$q}%",
            ]
        );

        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }
}