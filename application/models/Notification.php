<?php
use \Framework\Model;

class Notification extends Model
{
    const TABLE_NAME = 'notification';

    public static function addNotificationsForUsers(array $idArray, $text)
    {
        foreach ($idArray as $key => $id) {
            $idArray[$key] = "({$id}, :text)";
        }
        $idArray = implode(',', $idArray);

        $sql = "INSERT INTO ".self::TABLE_NAME.
               " (target_id, text)".
               " VALUES {$idArray}";

        $query = self::getConnection()->prepare($sql);

        $query->execute([':text' => $text]);
    }

    public static function numberOfNotificationForUser($userId)
    {
        $sql = "SELECT COUNT(*) AS numberOfNotifications from ".self::TABLE_NAME.
               " WHERE target_id = ? AND revised = 0";

        $query = self::getConnection()->prepare($sql);
        $query->execute([$userId]);

        $result = $query->fetch(PDO::FETCH_ASSOC);
         return $result['numberOfNotifications'];
    }

    public static function getNotificationById($notificationId)
    {
        $sql = "SELECT * FROM ".self::TABLE_NAME.
               " WHERE id = ?";

        $query = self::getConnection()->prepare($sql);
        $query->execute([$notificationId]);

        $result = $query->fetch(PDO::FETCH_ASSOC);

        return $result;
    }

    public static function getNotificationsForUser($userId)
    {
        $sql = "SELECT id, text FROM ".self::TABLE_NAME.
               " WHERE target_id = ? AND revised = 0";

        $query = self::getConnection()->prepare($sql);
        $query->execute([$userId]);

        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    public static function deleteNotification($notificationId)
    {
        $sql = "UPDATE ".self::TABLE_NAME.
               " SET revised = 1".
               " WHERE id = ?";

        $query = self::getConnection()->prepare($sql);
        $query->execute([$notificationId]);
    }
}