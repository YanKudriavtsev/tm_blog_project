<?php
use Framework\Model;

class Comment extends Model
{
    const TABLE_NAME = 'comment';

    public static function getCommentsByPostId($postId)
    {
        $sql = "SELECT c.*, u.first_name, u.last_name FROM ".self::TABLE_NAME." AS c  ".
               "INNER JOIN user AS u ON c.author_id = u.id ".
               "WHERE c.post_id=? ".
               "ORDER BY c.created_at DESC";

        $query = self::getConnection()->prepare($sql);
        $query->execute([$postId]);

        $result = $query->fetchAll(PDO::FETCH_GROUP);

        return $result;
    }

    public static function save($data)
    {
        $sql = "INSERT INTO ".self::TABLE_NAME." (author_id, post_id, text, reply_to) VALUES (?, ?, ?, ?)";
        $query = self::getConnection()->prepare($sql);

        // If the user does not respond to the comment - null, otherwise - id comment
        $replyComment = $data['replyCommentId'] == '' ? null: $data['replyCommentId'];

        $result = $query->execute(
            [
                $data['commentAuthorId'],
                $data['postId'],
                $data['commentText'],
                $replyComment
            ]
        );

        return $result;
    }
}