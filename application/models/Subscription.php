<?php
use \Framework\Model;

class Subscription extends Model
{
    const TABLE_NAME = 'subscription';

    public static function subscribe($subscriberId, $targetId)
    {
        $sql = "INSERT INTO ".self::TABLE_NAME.
               " (subscriber_id, target_id)".
               " VALUES (?, ?)";

        $query = self::getConnection()->prepare($sql);
        $result = $query->execute([$subscriberId, $targetId]);

        return $result;
    }

    public static function unsubscribe($subscriberId, $targetId)
    {
        $sql = "DELETE FROM ".self::TABLE_NAME.
               " WHERE subscriber_id = ? AND target_id = ?";

        $query = self::getConnection()->prepare($sql);
        $result = $query->execute([$subscriberId, $targetId]);

        return $result;
    }

    public static function getSubscription($subscriberId, $targetId)
    {
        $sql = "SELECT id FROM ".self::TABLE_NAME.
               " WHERE subscriber_id = ? AND target_id = ?";

        $query = self::getConnection()->prepare($sql);
        $query->execute([$subscriberId, $targetId]);

        $result = $query->fetch(PDO::FETCH_ASSOC);

        return $result;
    }

    public static function getUserSubscriptions($userId)
    {
	    $sql = "SELECT u.id, u.first_name, u.last_name, u.email FROM user AS u ".
		       "INNER JOIN ".self::TABLE_NAME." AS s ".
               "ON s.subscriber_id = ? ".
	           "WHERE u.id = s.target_id";

        $query = self::getConnection()->prepare($sql);
        $query->execute([$userId]);

        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    public static function getUserSubscribers($userId)
    {
        $sql = "SELECT subscriber_id FROM ".self::TABLE_NAME.
		       " WHERE target_id = ?";

        $query = self::getConnection()->prepare($sql);
        $query->execute([$userId]);

        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        return array_column($result, 'subscriber_id');
    }
}