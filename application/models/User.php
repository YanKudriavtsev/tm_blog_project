<?php
use Framework\Model;

class User extends Model
{
    const TABLE_NAME = 'user';

    public static function getByEmail($email)
    {
        $sql = "SELECT * FROM ".self::TABLE_NAME.
               " WHERE email = ?";

        $query = self::getConnection()->prepare($sql);
        $query->execute([$email]);

        $user = $query->fetch(PDO::FETCH_ASSOC);

        return $user;
    }

    public static function getById($id)
    {
        $sql = "SELECT * FROM ".self::TABLE_NAME.
               " WHERE id = ?";

        $query = self::getConnection()->prepare($sql);
        $query->execute([$id]);

        $user = $query->fetch(PDO::FETCH_ASSOC);

        return $user;
    }

    public static function updateById($id, $user)
    {
        $sql = "UPDATE ".self::TABLE_NAME.
            " SET first_name = ?, last_name = ?, email = ?".
            " WHERE id=?";

        $query = self::getConnection()->prepare($sql);

        $query->execute([
            $user['first_name'],
            $user['last_name'],
            $user['email'],
            $id]);
    }

    public static function save($user)
    {
        $sql = "INSERT INTO ".self::TABLE_NAME.
               " (first_name, last_name, email, password)".
               " VALUES (?, ?, ?, ?)";

        $query = self::getConnection()->prepare($sql);

        $query->execute([
            "{$user['first_name']}",
            "{$user['last_name']}",
            "{$user['email']}",
            "{$user['password']}"
        ]);

        return self::getConnection()->lastInsertId();
    }
}