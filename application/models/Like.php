<?php
use \Framework\Model;

class Like extends Model
{
    const TABLE_NAME = 'likes_of_posts';

    public static function addLike($postId, $userId)
    {
        $sql = "INSERT INTO ".self::TABLE_NAME.
               " (owner_id, post_id)".
               " VALUES (?, ?)";

        $query = self::getConnection()->prepare($sql);
        $query->execute(
            [
                $userId,
                $postId,
            ]
        );
    }

    public static function removeLike($postId, $userId)
    {
        $sql = "DELETE FROM ".self::TABLE_NAME.
               " WHERE owner_id = ? AND post_id = ?";

        $query = self::getConnection()->prepare($sql);
        $query->execute(
            [
                $userId,
                $postId,
            ]
        );
    }

    public static function getLikeByPostAndUserId($postId, $userId)
    {
        $sql = "SELECT id FROM ".self::TABLE_NAME.
               " WHERE owner_id = ? AND post_id = ?";

        $query = self::getConnection()->prepare($sql);
        $query->execute(
            [
                $userId,
                $postId,
            ]
        );

        $result = $query->fetch(PDO::FETCH_ASSOC);

        return $result;
    }
}