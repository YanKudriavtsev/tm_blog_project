<?php
use Framework\Controller;

class CommentController extends Controller
{

    public function addAction()
    {
        if (isset($_POST['submit'])) {

            Comment::save($_POST);

            $this->redirect("/post/" . $_POST['postId']);
        }
    }
}