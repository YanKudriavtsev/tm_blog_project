<?php
use \Framework\Controller;

class ErrorController extends Controller
{
    public function getErrorText($errorCode) {
        $errors = [
            '404' => '404. Page not found',
        ];
        return $errors[$errorCode];
    }

    public function showAction($errorCode) {
        $errorText = $this->getErrorText($errorCode);
        $this->view->render(
            'layout.php',
            'error/404.php',
            [
                'errorText' => $errorText,
            ]
        );
    }
}