<?php
use \Framework\Controller;

class NotificationController extends Controller
{
    public function deleteAction($notificationId)
    {
        $notification = Notification::getNotificationById($notificationId);

        // If user try to delete not his notification - redirects to error page
        if ($notification['target_id'] == AuthHelper::getAuthenticatedUser()['id']) {
            Notification::deleteNotification($notificationId);
            $this->redirect('/user/subscriptions');
        } else {
            $this->redirect('/error/show/404');
        }
    }
}