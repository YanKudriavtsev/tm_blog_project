<?php
use Framework\Controller;

class PostController extends Controller
{
    // showing most popular and newest posts
    public function postsAction()
    {
        $page = isset($_GET['page'])? $_GET['page']: 1;
        $newestPosts = Post::getAll($page, 'created_at');
        $mostPopularPosts = Post::getAll($page, 'likes');

        $this->view->render(
            'layout.php',
            'post/posts.php',
            [
                'newestPosts' => $newestPosts,
                'mostPopularPosts' => $mostPopularPosts,
                'pageHeader' => 'All posts',
                // variable for paginator
                'page' => $page,
            ]
        );
    }

    // showing specific post
    public function getAction($postId)
    {
        $post = Post::getById($postId);

        // if user requested non-existing post - redirect to error page
        if (empty($post)) {
            $this->redirect('/error/show/404');
        }

        $comments = Comment::getCommentsByPostId($postId);
        $postCategories = Category::getCategoriesByPostId($postId);

        /*
        * All tags of post are kept in database as text field.
        * That code make tags as links
        */
        $arrayOfTags = explode(' ', $post['tags']);
        $tagsAsLinks = '';
        foreach ($arrayOfTags as $tag) {
            $tagsAsLinks .= "<a href='/posts/tag/{$tag}'>#{$tag} </a>";
        }
        $post['tags'] = $tagsAsLinks;

        // Different class of icon depending on whether the user has "liked" the post
        $likeIconClass = LikeHelper::userLikedPost($postId)? 'unliked-icon': 'liked-icon';

        $this->view->render(
            'layout.php',
            'post/post.php',
            [
                'post' => $post,
                'comments' => $comments,
                'postCategories' => $postCategories,
                'pageHeader' => $post['title'],
                'likeIconClass' => $likeIconClass,
            ]
        );
    }

    public function getByCategoryIdAction($categoryId)
    {
        $page = isset($_GET['page'])? $_GET['page']: 1;
        $newestPosts = Post::getByCategoryId($categoryId, $page, 'created_at');
        $mostPopularPosts = Post::getByCategoryId($categoryId, $page, 'likes');

        $categoryName = Category::getById($categoryId)['name'];

        $this->view->render(
            'layout.php',
            'post/posts.php',
            [
                'newestPosts' => $newestPosts,
                'mostPopularPosts' => $mostPopularPosts,
                'pageHeader' => "Posts of category '{$categoryName}'",
                // variable for paginator
                'page' => $page,
            ]);
    }

    public function addAction()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $postId = Post::save($_POST);

            // Name of user directory - his id
            $userDirectoy = AuthHelper::getAuthenticatedUser()['id'];
            ImageHelper::upload($_FILES['postPicture'], $postId, $userDirectoy);

            // If user do not selected category - do not save categories for post
            if (isset($_POST['postCategory'])) {
                Category::addPostToCategory($_POST['postCategory'], $postId);
            }

            // Chooses subscribers of user and sending them notifications
            $userSubscribers = Subscription::getUserSubscribers($_POST['postAuthorId']);
            if ($userSubscribers) {
                $notificationText = "New post - <a href='/post/{$postId}'>{$_POST['postTitle']}</a>";
                Notification::addNotificationsForUsers($userSubscribers, $notificationText);
            }

            $this->redirect("/post/{$postId}/");
        } else {
            $this->view->render(
                'layout.php',
                'post/addPost.php',
                []
            );
        }
    }

    public function editAction($postId)
    {
        $post = Post::getById($postId);

        // If user trying to change not his post - redirect to error page
        if (AuthHelper::getAuthenticatedUser()['id'] != $post['author']) {
            $this->redirect('/error/show/404');
        } else {
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                /*
                * First the preview image for post has been deleted (soft delete),
                * and then saved new image
                */
                Post::update($_POST);
                Image::deletePreviewImage($postId);
                $userDirectoy = AuthHelper::getAuthenticatedUser()['id'];
                ImageHelper::upload($_FILES['postPicture'], $postId, $userDirectoy);

                // If user do selected category - this post adding to category
                if (isset($_POST['postCategory'])) {
                    Category::deletePostFromCategories($postId);
                    Category::addPostToCategory($_POST['postCategory'], $postId);
                }

                $this->redirect("/post/{$post['id']}");
            } else {
                /*
                * Selecting post categories.
                * Post categories has be to mark in template (checkbox)
                */
                $idOfPostCategories = array_column(
                    Category::getCategoriesByPostId($postId),
                    'id'
                );

                $this->view->render(
                    'layout.php',
                    'post/editPost.php',
                    [
                        'post' => $post,
                        'idOfPostCategories' => $idOfPostCategories,
                    ]
                );
            }
        }
    }

    public function deleteAction($postId)
    {
        Post::deleteById($postId);
        $this->redirect('/post/user-posts/admin');
    }

    public function userPostsAction($userId)
    {
        $page = isset($_GET['page'])? $_GET['page']: 1;
        $user = User::getById($userId);

        $newestPosts = Post::getByAuthorId($userId, $page, 'created_at');
        $mostPopularPosts = Post::getByAuthorId($userId, $page, 'likes');

        $this->view->render(
            'layout.php',
            'post/posts.php',
            [
                'newestPosts' => $newestPosts,
                'mostPopularPosts' => $mostPopularPosts,
                'pageHeader' => "Posts of {$user['first_name']} {$user['last_name']}",
                // variable for paginator
                'page' => $page,
            ]
        );
    }

    // This "action" showing all user posts and allows to delete or edit them
    public function userPostsAdminAction()
    {
        $page = isset($_GET['page'])? $_GET['page']: 1;
        $user = AuthHelper::getAuthenticatedUser();

        $posts = Post::getByAuthorId($user['id'], $page, 'created_at');

        $this->view->render(
            'layout.php',
            'post/userPostsAdmin.php',
            [
                'posts' => $posts,
                'pageHeader' => "Posts of {$user['first_name']} {$user['last_name']}",
                // variable for paginator
                'page' => $page,
            ]
        );
    }

    public function postsByTagAction($tag)
    {
        $page = isset($_GET['page'])? $_GET['page']: 1;
        $newestPosts = Post::getByTag($tag, $page, 'created_at');
        $mostPopularPosts = Post::getByTag($tag, $page, 'likes');

        $this->view->render(
            'layout.php',
            'post/posts.php',
            [
                'newestPosts' => $newestPosts,
                'mostPopularPosts' => $mostPopularPosts,
                'pageHeader' => "Posts with tag '#{$tag}'",
                // variable for paginator
                'page' => $page,
            ]
        );
    }
}