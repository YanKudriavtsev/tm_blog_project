<?php
use \Framework\Controller;

class LikeController extends Controller
{
    /*
     * If user do not liked the post, then like adds to post
     * Else like removed from post
     */
    public function addAction($postId)
    {
        $userId = AuthHelper::getAuthenticatedUser()['id'];

        if (!LikeHelper::userLikedPost($postId)) {
            Like::addLike($postId, $userId);
        } else {
            Like::removeLike($postId, $userId);
        }

        $this->redirect($_SERVER['HTTP_REFERER']);
    }
}