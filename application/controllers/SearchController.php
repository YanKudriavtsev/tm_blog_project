<?php
use \Framework\Controller;

class SearchController extends Controller
{
    public function searchAction()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $q = $_POST['q'];

            $tagsResult = Search::tagSearch($q);
            $usersResult = Search::userSearch($q);
            $postsResult = Search::postSearch($q);

            $this->view->render(
                'layout.php',
                'search/showResult.php',
                [
                    'pageHeader' => "Search as '{$q}'",
                    'tagsResult' => $tagsResult,
                    'usersResult' => $usersResult,
                    'postsResult' => $postsResult,
                ]
            );
        } else {
            $this->redirect('/error/show/404');
        }
    }
}