<?php
use \Framework\Controller;

class SubscriptionController extends Controller
{
    /*
    * Subscription to the author.
    * Redirects to the page from which the user came
    */
    public function subscribeAction($targetId)
    {
        $subscriberId = AuthHelper::getAuthenticatedUser()['id'];

        // Does not allow the user to subscribe himself
        if ($targetId != $subscriberId) {
                Subscription::subscribe($subscriberId, $targetId);
        }

        $this->redirect($_SERVER['HTTP_REFERER']);
    }

    /*
    * Unsubscribe from the author.
    * Redirects to the page from which the user came
    */
    public function unsubscribeAction($targetId)
    {
        // Checking if user has subscribed to author
        if (SubscriptionsHelper::userIsSubscribed($targetId)) {
                $subscriberId = AuthHelper::getAuthenticatedUser()['id'];
                Subscription::unsubscribe($subscriberId, $targetId);

                $this->redirect($_SERVER['HTTP_REFERER']);
        } else {
                $this->redirect('/error/show/404');
        }
    }

    // Showing user's subscriptions and user's notifications
    public function subscriptionsAction()
    {
        $userId = AuthHelper::getAuthenticatedUser()['id'];

        $userSubscriptions = Subscription::getUserSubscriptions($userId);
        $userNotifications = Notification::getNotificationsForUser($userId);

        $this->view->render(
                'layout.php',
            'user/subscriptions.php',
                [
                    'pageHeader' => 'Your subscriptions',
                    'userSubscriptions' => $userSubscriptions,
                    'userNotifications' => $userNotifications
                ]
        );
    }
}