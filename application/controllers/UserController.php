<?php
use \Framework\Controller;

class UserController extends Controller
{
    public function editAction()
    {
        $userId = AuthHelper::getAuthenticatedUser()['id'];

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $user = $_POST['user'];
            User::updateById($userId, $user);

            $this->redirect('/user/edit');
        } else {
            $user = User::getById($userId);
            $this->view->render(
                'layout.php',
                'user/edit_user.php',
                [
                    'user' => $user,
                ]
            );
        }
    }

    public function registerAction()
    {
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            $userData = $_POST['user'];

            $validator = new FormValidatorHelper();

            $validator->checkEmail($userData['email']);
            $validator->checkEmailExists($userData['email'], 'register');
            $validator->checkName([
                'first name' => $userData['first_name']
            ]);
            $validator->checkName([
                'last name' => $userData['last_name']
            ]);
            $validator->checkPasswordFormat(
                $userData['password'],
                $userData['confirm_password']
            );

            $validationErrors = $validator->getErrors();
            /*
            * If the user has a registration error, the form is displayed again
            * The fields "First name", "Last name" and "E-mail"
            * are filled in with the obtained values
            */
            if (count($validationErrors) > 0) {
                $this->view->render(
                    'layout.php',
                    'user/register_user.php',
                    [
                        'errors' => $validationErrors,
                        // Some fields of form have values if user has errors during registration
                        // (first name, last name, email)
                        'inputData' => $userData,
                    ]);
            } else {
                // Hashing user password
                $userData['password'] = password_hash(trim($userData['password']), PASSWORD_DEFAULT);

                $userId = User::save($userData);
                // Directory for "preview images" of user posts
                // Directory name - user id
                mkdir(ROOT . '/web/userFiles/' . $userId);

                $this->redirect('/user/login');
            }

        } else {
            /*
            * The form displays "First name", "Last name", "Email" if user
            * has errors during registration.
            * The "inputData" array is used, when the form is first time displayed (GET request)
            */
            $inputData = ['first_name' => null, 'last_name' => null, 'email' => null];

            $this->view->render(
                'layout.php',
                'user/register_user.php',
                [
                    'pageHeader' => 'Registration',
                    'inputData' => $inputData
                ]
            );
        }
    }

    public function loginAction()
    {
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            $inputUserData = $_POST['user'];

            $validator = new FormValidatorHelper();

            $validator->checkEmail($inputUserData['email']);
            $validator->checkEmailExists($inputUserData['email'], 'login');
            $validator->checkPassword(
                $inputUserData['password'],
                $inputUserData['email']
            );

            $validationErrors = $validator->getErrors();

            if (count($validationErrors) > 0) {
                $this->view->render(
                    'layout.php',
                    'user/login_user.php',
                    [
                        'errors' => $validationErrors,
                    ]
                );
            } else {
                $user = User::getByEmail($inputUserData['email']);

                $_SESSION['user'] = [
                    'first_name' => $user['first_name'],
                    'last_name' => $user['last_name'],
                    'email' => $user['email'],
                    'id' => $user['id'],
                ];
                $this->redirect('/');
            }

        } else {
            $this->view->render(
                'layout.php',
                'user/login_user.php',
                [
                    'pageHeader' => 'Login',
                ]
            );
        }
    }

    public function logoutAction()
    {
        unset($_SESSION['user']);
        $this->redirect('/');
    }
}
