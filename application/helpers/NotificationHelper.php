<?php

class NotificationHelper
{
    // Returns number of user notifications. For anonymous users returns 0
    public static function getNumberOfNotificationForUser()
    {
        if (AuthHelper::userIsAuthenticated()) {
            $userId = AuthHelper::getAuthenticatedUser()['id'];
            $result = Notification::numberOfNotificationForUser($userId);
            return $result;
        }
        return 0;
    }
}