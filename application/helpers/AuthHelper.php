<?php

class AuthHelper
{
    /*
     * This method is used in views to display different
     * content for authorized and unauthorized users.
     */
    public static function userIsAuthenticated()
    {
        if (isset($_SESSION['user'])) {
            return true;
        }
        return false;
    }

    // returns authenteficated user or False
    public static function getAuthenticatedUser()
    {
        if (isset($_SESSION['user'])) {
            return $_SESSION['user'];
        }
        return false;
    }

}