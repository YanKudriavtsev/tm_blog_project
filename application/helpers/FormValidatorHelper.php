<?php

class FormValidatorHelper
{
    private $validationErrors;

    public function __construct()
    {
        $this->validationErrors = [];
    }

    public function checkEmail(string $email)
    {
        $emailIsCorrect = filter_var($email, FILTER_VALIDATE_EMAIL);
        if (!$emailIsCorrect) {
            $this->validationErrors[] = 'Невірний формат email';
        }
    }

    public function checkEmailExists($email, $flag='login')
    {
        $user = User::getByEmail($email);
        if ($flag == 'login') {
            if (!$user) {
                $this->validationErrors[] = 'Користувача з таким email не існує';
            }
        } elseif ($flag == 'register') {
            if ($user) {
                $this->validationErrors[] = 'Користувач з таким email вже існує';
            }
        }
    }

    /*
     * input: array, where key - name of field, value - value of field
     * ['first_name' => 'John']
     */
    public function checkName(array $data)
    {
        $nameOfField = array_keys($data)[0];
        $valueOfField = $data[$nameOfField];

        if (empty($valueOfField)) {
            $this->validationErrors[] = "Поле {$nameOfField} є обов'язковим";
        }
    }

    public function checkPasswordFormat(string $password, string $confirmedPassword)
    {
        if ($password != $confirmedPassword) {
            $this->validationErrors[] = 'Паролі не співпадають';
        } else {
            $passwordLength = strlen($password);
            if ($passwordLength < 6 or $passwordLength > 16) {
                $this->validationErrors[] = "Довжина паролю має бути від 6 до 16 символів";
            }
        }
    }

    public function checkPassword(string $inputPassword, string $userEmail)
    {
        $user = User::getByEmail($userEmail);
        if (!password_verify($inputPassword, $user['password'])) {
            $this->validationErrors[] = 'Невірний пароль';
        }

    }

    public function getErrors()
    {
        return $this->validationErrors;
    }
}