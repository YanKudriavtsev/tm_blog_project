<?php

class LikeHelper
{
    /*
     * If user liked the post - method returns true,
     * else - returns false
     */
    public static function userLikedPost($postId)
    {
        if (AuthHelper::userIsAuthenticated()){
            $userId = AuthHelper::getAuthenticatedUser()['id'];
            if (Like::getLikeByPostAndUserId($postId, $userId)) {
                return true;
            }
        }
        return false;
    }
}