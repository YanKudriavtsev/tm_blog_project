<?php

class ImageHelper
{
    public static function upload($newImage, $postId, $userDirectory)
    {
        if (isset($newImage)) {
            $tmpName = $newImage['tmp_name'];
            $fileName = time() . $newImage['name'];

            $imageLocation = ROOT . "/web/userFiles/{$userDirectory}/{$fileName}";

            // Image path for rendering in web page
            $imagePath = "/web/userFiles/{$userDirectory}/{$fileName}";

            move_uploaded_file($tmpName, $imageLocation);

            Image::save($imagePath, $postId);
        }
    }
}
