<?php

class SubscriptionsHelper
{
    // Returns true if user has been subscribed to author
    public static function userIsSubscribed($targetId)
    {
        if(AuthHelper::userIsAuthenticated()) {
            $userId = AuthHelper::getAuthenticatedUser()['id'];

            if(Subscription::getSubscription($userId, $targetId)) {
                return true;
            }
        }
        return false;
    }
}