<?php

class UrlHelper
{
    public static function getUrl()
    {
        //URL without GET parameters
        $uri = explode('?', $_SERVER['REQUEST_URI'])[0];

        // Deletes the last slash except the path "/" (root)
        if ($uri != '/') {
            $url = rtrim($uri, '/');
        } else {
            $url = $uri;
        }

        return $url;
    }
}